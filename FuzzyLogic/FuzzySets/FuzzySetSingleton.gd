extends FuzzySet
class_name FuzzySetSingleton

#-----------------------------------------------------------------------------
#    This defines a fuzzy set that is a singleton (a range
#    over which the DOM is always 1.0)
#-----------------------------------------------------------------------------

# the values that define the shape of this FLV
var midPoint : float
var leftOffset : float
var rightOffset : float

func _init(mid : float, lft : float, rgt : float).(mid):
    midPoint = mid
    leftOffset = lft
    rightOffset = rgt

# this method calculates the degree of membership for a particular value
func CalculateDOM(val : float) -> float:
    if (val >= midPoint - leftOffset and val <= midPoint + rightOffset):
        return 1.0
    # out of range of this FLV, return zero
    else:
        return 0.0
