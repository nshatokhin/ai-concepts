extends FuzzySet
class_name FuzzySetLeftShoulder

#-----------------------------------------------------------------------------
#   Definition of a fuzzy set that has a left shoulder shape. (the
#   minimum value this variable can accept is *any* value less than the
#   midpoint.
#-----------------------------------------------------------------------------


# the values that define the shape of this FLV
var peakPoint : float
var rightOffset : float
var leftOffset : float

func _init(peak : float, LeftOffset : float, RightOffset : float).( ((peak - LeftOffset) + peak) / 2):
	peakPoint = peak
	leftOffset = LeftOffset
	rightOffset = RightOffset


# this method calculates the degree of membership for a particular value
func CalculateDOM(val : float) -> float:
	# test for the case where the left or right offsets are zero
	# (to prevent divide by zero errors below)
	if (Utils.isEqual(rightOffset, 0.0) and Utils.isEqual(peakPoint, val)) or (Utils.isEqual(leftOffset, 0.0) and Utils.isEqual(peakPoint, val)):
		return 1.0
	
	# find DOM if right of center
	elif val >= peakPoint and val < peakPoint + rightOffset:
		var grad : float = 1.0 / -rightOffset
	
		return grad * (val - peakPoint) + 1.0
	
	# find DOM if left of center
	elif val < peakPoint and val >= peakPoint - leftOffset:
		return 1.0
	
	# out of range of this FLV, return zero
	else:
		return 0.0
