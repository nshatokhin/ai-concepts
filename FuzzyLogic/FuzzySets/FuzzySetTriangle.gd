extends FuzzySet
class_name FuzzySetTriangle

#-----------------------------------------------------------------------------
#
#    This is a simple class to define fuzzy sets that have a triangular 
#    shape and can be defined by a mid point, a left displacement and a
#    right displacement. 
#-----------------------------------------------------------------------------

# the values that define the shape of this FLV
var peakPoint : float
var leftOffset : float
var rightOffset : float
  
func _init(mid : float, lft : float, rgt : float).(mid):
	peakPoint = mid
	leftOffset = lft
	rightOffset = rgt


# this method calculates the degree of membership for a particular value
func CalculateDOM(val : float) -> float:
	# test for the case where the triangle's left or right offsets are zero
	# (to prevent divide by zero errors below)
	if (Utils.isEqual(rightOffset, 0.0) and Utils.isEqual(peakPoint, val)) or (Utils.isEqual(leftOffset, 0.0) and Utils.isEqual(peakPoint, val)):
		return 1.0
	
	# find DOM if left of center
	if val <= peakPoint and val >= peakPoint - leftOffset:
		var grad : float = 1.0 / leftOffset
		
		return grad * (val - (peakPoint - leftOffset))
	
	# find DOM if right of center
	elif val > peakPoint and val < peakPoint + rightOffset:
		var grad : float = 1.0 / -rightOffset
		
		return grad * (val - peakPoint) + 1.0
	# out of range of this FLV, return zero
	else:
		return 0.0
