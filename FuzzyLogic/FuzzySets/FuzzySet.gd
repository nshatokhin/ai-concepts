extends Reference
class_name FuzzySet

#-----------------------------------------------------------------------------
#   Class to define an interface for a fuzzy set
#-----------------------------------------------------------------------------

# this will hold the degree of membership of a given value in this set 
var DOM : float

# this is the maximum of the set's membership function. For instamce, if
# the set is triangular then this will be the peak point of the triangular.
# if the set has a plateau then this value will be the mid point of the 
# plateau. This value is set in the constructor to avoid run-time
# calculation of mid-point values.
var representativeValue : float

func _init(RepVal : float):
    DOM = 0.0
    representativeValue = RepVal

# return the degree of membership in this set of the given value. NOTE,
# this does not set m_dDOM to the DOM of the value passed as the parameter.
# This is because the centroid defuzzification method also uses this method
# to determine the DOMs of the values it uses as its sample points.
func CalculateDOM(val : float):
    return 0.0

# if this fuzzy set is part of a consequent FLV, and it is fired by a rule 
# then this method sets the DOM (in this context, the DOM represents a
# confidence level)to the maximum of the parameter value or the set's 
# existing DOM value
func ORwithDOM(val : float) -> void:
    if val > DOM:
        DOM = val

# accessor methods
func ClearDOM() -> void:
    DOM = 0.0
    
func GetDOM() -> float:
    return DOM
    
func SetDOM(val : float) -> void:
    if val > 1.0 or val < 0.0:
        print("<FuzzySet::SetDOM>: invalid value")
        return
    DOM = val
