extends FuzzyTerm
class_name FzSet

#-----------------------------------------------------------------------------
#
#   Class to provide a proxy for a fuzzy set. The proxy inherits from
#   FuzzyTerm and therefore can be used to create fuzzy rules
#
#-----------------------------------------------------------------------------

# a reference to the fuzzy set this proxy represents
var set : FuzzySet

func _init(fs : FuzzySet):
	set = fs

func Clone() -> FuzzyTerm:
	return get_script().new(set)
	
func GetDOM() -> float:
	return set.GetDOM()
	
func ClearDOM() -> void:
	set.ClearDOM()
	
func ORwithDOM(val) -> void:
	set.ORwithDOM(val)
