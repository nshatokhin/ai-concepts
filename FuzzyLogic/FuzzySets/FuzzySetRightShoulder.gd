extends FuzzySet
class_name FuzzySetRightShoulder

#-----------------------------------------------------------------------------
#    Definition of a fuzzy set that has a right shoulder shape. (the
#    maximum value this variable can accept is *any* value greater than 
#    the midpoint.
#-----------------------------------------------------------------------------

# the values that define the shape of this FLV
var peakPoint : float
var leftOffset : float
var rightOffset : float

func _init(peak : float, LeftOffset : float, RightOffset : float).( ((peak + RightOffset) + peak) / 2):
	peakPoint = peak
	leftOffset = LeftOffset
	rightOffset = RightOffset


# this method calculates the degree of membership for a particular value
func CalculateDOM(val : float) -> float:
	# test for the case where the left or right offsets are zero
	# (to prevent divide by zero errors below)
	if (Utils.isEqual(rightOffset, 0.0) and Utils.isEqual(peakPoint, val)) or (Utils.isEqual(leftOffset, 0.0) and Utils.isEqual(peakPoint, val)):
		return 1.0
	
	# find DOM if left of center
	elif val <= peakPoint and val > peakPoint - leftOffset:
		var grad : float = 1.0 / leftOffset
	
		return grad * (val - (peakPoint - leftOffset))

	# find DOM if right of center and less than center + right offset
	elif val > peakPoint and val <= peakPoint + rightOffset:
		return 1.0
	
	else:
		return 0.0
