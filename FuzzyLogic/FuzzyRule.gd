extends Reference
class_name FuzzyRule

#-----------------------------------------------------------------------------
#
#    This class implements a fuzzy rule of the form:
#  
#    IF fzVar1 AND fzVar2 AND ... fzVarn THEN fzVar.c
#
#-----------------------------------------------------------------------------

# antecedent (usually a composite of several fuzzy sets and operators)
var antecedent : FuzzyTerm

# consequence (usually a single fuzzy set, but can be several ANDed together)
var consequence : FuzzyTerm

# it doesn't make sense to allow clients to copy rules
#  FuzzyRule(const FuzzyRule&);
#  FuzzyRule& operator=(const FuzzyRule&);

func _init(ant : FuzzyTerm, con : FuzzyTerm):
	antecedent = ant.Clone()
	consequence = con.Clone()

#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		antecedent.free()
#		consequence.free()

func SetConfidenceOfConsequentToZero() -> void:
	consequence.ClearDOM()

# this method updates the DOM (the confidence) of the consequent term with
# the DOM of the antecedent term. 
func Calculate() -> void:
	consequence.ORwithDOM(antecedent.GetDOM())
