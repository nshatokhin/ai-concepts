extends Reference
class_name FuzzyTerm

#-----------------------------------------------------------------------------
#   Abstract class to provide an interface for classes able to be
#  used as terms in a fuzzy if-then rule base.
#-----------------------------------------------------------------------------

# all terms must implement a virtual constructor
func Clone() -> FuzzyTerm:
	return get_script().new()

# retrieves the degree of membership of the term
func GetDOM() -> float:
	return 0.0

# clears the degree of membership of the term
func ClearDOM() -> void:
	pass

# method for updating the DOM of a consequent when a rule fires
func ORwithDOM(val : float) -> void:
	pass
