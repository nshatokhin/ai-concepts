extends Reference
class_name FuzzyModule

#-----------------------------------------------------------------------------
#
#    This class describes a fuzzy module: a collection of fuzzy variables
#    and the rules that operate on them.
#
#-----------------------------------------------------------------------------
 

# you must pass one of these values to the defuzzify method. This module
# only supports the MaxAv and centroid methods.
enum DefuzzifyMethod {
	MaxAV
	Centroid
}

# when calculating the centroid of the fuzzy manifold this value is used
# to determine how many cross-sections should be sampled
enum {
	NumSamples = 15
}

# a map of all the fuzzy variables this module uses
var variables : Dictionary = {} # std::map<std::string, FuzzyVariable*>

# a vector containing all the fuzzy rules
var rules : Array = []


#///////////////////////////////////////////////////////////////////////////////

#----------------------------- Fuzzify ---------------------------------------
#
#   this method calls the Fuzzify method of the variable with the same name
#   as the key
#-----------------------------------------------------------------------------
func Fuzzify(NameOfFLV : String, val : float) -> void:
	# first make sure the key exists
	if not variables.has(NameOfFLV):
		print("<FuzzyModule::Fuzzify>:key not found")
		return
	
	variables[NameOfFLV].Fuzzify(val)


#---------------------------- DeFuzzify --------------------------------------
#
#   given a fuzzy variable and a deffuzification method this returns a 
#   crisp value
#-----------------------------------------------------------------------------
func DeFuzzify(NameOfFLV : String, method : int = DefuzzifyMethod.max_av) -> float:
	# first make sure the key exists
	if not variables.has(NameOfFLV):
		print("<FuzzyModule::DeFuzzifyMaxAv>:key not found")
		return 0.0
	
	# clear the DOMs of all the consequents of all the rules
	SetConfidencesOfConsequentsToZero()
	
	# process the rules
	for curRule in rules:
		curRule.Calculate()
	
	# now defuzzify the resultant conclusion using the specified method
	match method:
		DefuzzifyMethod.Centroid:
			return variables[NameOfFLV].DeFuzzifyCentroid(NumSamples)
		DefuzzifyMethod.MaxAV:
			return variables[NameOfFLV].DeFuzzifyMaxAv()
	
	return 0.0


#-------------------------- ClearConsequents ---------------------------------
#
#   zeros the DOMs of the consequents of each rule
#-----------------------------------------------------------------------------
func SetConfidencesOfConsequentsToZero() -> void:
	for curRule in rules:
		curRule.SetConfidenceOfConsequentToZero()

#------------------------------ dtor -----------------------------------------
#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		for curVar in variables.keys():
#			variables[curVar].free()
#		
#		for curRule in rules:
#			curRule.free()


#----------------------------- AddRule ---------------------------------------
# adds a rule to the module
func AddRule(antecedent : FuzzyTerm, consequence : FuzzyTerm) -> void:
	rules.push_back(FuzzyRule.new(antecedent, consequence))

 
#-------------------------- CreateFLV ---------------------------
#
#   creates a new fuzzy variable and returns a reference to it
#-----------------------------------------------------------------------------
func CreateFLV(VarName : String) -> FuzzyVariable:
	variables[VarName] = FuzzyVariable.new()
	
	return variables[VarName]


#---------------------------- WriteAllDOMs -----------------------------------
# writes the DOMs of all the variables in the module to an output string
func GetAllDOMsText() -> String:
	var text : String = ""
	
	for curVar in variables.keys():
		text += curVar + ": "
		text += variables[curVar].GetDOMsText()
		text += "\n"
	
	return text
