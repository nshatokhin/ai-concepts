extends FuzzyTerm
class_name FzVery

#-----------------------------------------------------------------------------
#    Class to implement fuzzy hedge FzVery
#-----------------------------------------------------------------------------

var set : FuzzySet

# prevent copying and assignment by clients
# TODO

func _init(ft):
	set = ft.set

func GetDOM() -> float:
	return set.GetDOM() * set.GetDOM()

func Clone() -> FuzzyTerm:
	return get_script().new(self)

func ClearDOM() -> void:
	set.ClearDOM()
	
func ORwithDOM(val : float) -> void:
	set.ORwithDOM(val * val)
