extends FuzzyTerm
class_name FzFairly

#-----------------------------------------------------------------------------
#    Class to implement fuzzy hedge FzFairly
#-----------------------------------------------------------------------------

var set : FuzzySet

# prevent copying and assignment
# TODO
func _init(inst : FzFairly):
    set = inst.set

func _init(ft : FzSet):
    set = ft.set

func GetDOM() -> float:
    return sqrt(set.GetDOM())

func Clone() -> FuzzyTerm:
    return FzFairly.new(self)

func ClearDOM() -> void:
    set.ClearDOM()
    
func ORwithDOM(val : float) -> void:
    set.ORwithDOM(sqrt(val))
