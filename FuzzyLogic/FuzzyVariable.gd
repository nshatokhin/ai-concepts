extends Reference
class_name FuzzyVariable

#-----------------------------------------------------------------------------
#
#    Class to define a fuzzy linguistic variable (FLV).
#
#   An FLV comprises of a number of fuzzy sets  
#
#-----------------------------------------------------------------------------

# disallow copies
# TODO
#  FuzzyVariable(const FuzzyVariable&);
#  FuzzyVariable& operator=(const FuzzyVariable&);
 
# a map of the fuzzy sets that comprise this variable
var memberSets : Dictionary = {}

# the minimum and maximum value of the range of this variable
var minRange : float
var maxRange : float
  
#---------------------------- AdjustRangeToFit -------------------------------
#
# this method is called with the upper and lower bound of a set each time a
# new set is added to adjust the upper and lower range values accordingly
#-----------------------------------------------------------------------------
func AdjustRangeToFit(minBound : float, maxBound : float) -> void:
	if minBound < minRange:
		minRange = minBound
	if maxBound > maxRange:
		maxRange = maxBound


# a client retrieves a reference to a fuzzy variable when an instance is
# created via FuzzyModule::CreateFLV(). To prevent the client from deleting
# the instance the FuzzyVariable destructor is made private and the 
# FuzzyModule class made a friend.
# TODO
#  ~FuzzyVariable();

#  friend class FuzzyModule;

func _init():
	minRange = 0.0
	maxRange = 0.0
  
# the following methods create instances of the sets named in the method
# name and add them to the member set map. Each time a set of any type is
# added the m_dMinRange and m_dMaxRange are adjusted accordingly. All of the
# methods return a proxy class representing the newly created instance. This
# proxy set can be used as an operand when creating the rule base.

#------------------------- AddTriangularSet ----------------------------------
#
#   adds a triangular shaped fuzzy set to the variable
#-----------------------------------------------------------------------------
func AddTriangularSet(name : String, minBound : float, peak : float, maxBound : float) -> FzSet:
	memberSets[name] = FuzzySetTriangle.new(peak, peak - minBound, maxBound - peak)
	# adjust range if necessary
	AdjustRangeToFit(minBound, maxBound)
	
	return FzSet.new(memberSets[name])
	

#--------------------------- AddLeftShoulder ---------------------------------
#
#   adds a left shoulder type set
#-----------------------------------------------------------------------------
func AddLeftShoulderSet(name : String, minBound : float, peak : float, maxBound : float) -> FzSet:
	memberSets[name] = FuzzySetLeftShoulder.new(peak, peak - minBound, maxBound - peak);
	
	# adjust range if necessary
	AdjustRangeToFit(minBound, maxBound)
	
	return FzSet.new(memberSets[name])


#--------------------------- AddRightShoulder ---------------------------------
#
#   adds a left shoulder type set
#-----------------------------------------------------------------------------
func AddRightShoulderSet(name : String, minBound : float, peak : float, maxBound : float) -> FzSet:
	memberSets[name] = FuzzySetRightShoulder.new(peak, peak - minBound, maxBound - peak);
	
	# adjust range if necessary
	AdjustRangeToFit(minBound, maxBound)
	
	return FzSet.new(memberSets[name])


#--------------------------- AddSingletonSet ---------------------------------
#
#  adds a singleton to the variable
#-----------------------------------------------------------------------------
func AddSingletonSet(name : String, minBound : float, peak : float, maxBound : float) -> FzSet:
	memberSets[name] = FuzzySetSingleton.new(peak, peak - minBound, maxBound - peak)
	
	AdjustRangeToFit(minBound, maxBound)
	
	return FzSet.new(memberSets[name])


#--------------------------- Fuzzify -----------------------------------------
#
#   takes a crisp value and calculates its degree of membership for each set
#   in the variable.
#-----------------------------------------------------------------------------
func Fuzzify(val : float) -> void:
	# make sure the value is within the bounds of this variable
	if (val < minRange) or (val > maxRange):
		print("<FuzzyVariable::Fuzzify>: value out of range")
		return
	
	# for each set in the flv calculate the DOM for the given value
	for curSet in memberSets.keys():
		memberSets[curSet].SetDOM(memberSets[curSet].CalculateDOM(val))


#--------------------------- DeFuzzifyMaxAv ----------------------------------
#
# defuzzifies the value by averaging the maxima of the sets that have fired
#
# OUTPUT = sum (maxima * DOM) / sum (DOMs) 
#-----------------------------------------------------------------------------
func DeFuzzifyMaxAv() -> float:
	var bottom : float = 0.0
	var top : float = 0.0
	
	for curSet in memberSets.keys():
		bottom += memberSets[curSet].GetDOM()
		
		top += memberSets[curSet].representativeValue * memberSets[curSet].GetDOM()
	
	# make sure bottom is not equal to zero
	if Utils.isEqual(0, bottom):
		return 0.0
	
	return top / bottom
	

#------------------------- DeFuzzifyCentroid ---------------------------------
#
#   defuzzify the variable using the centroid method
#-----------------------------------------------------------------------------
func DeFuzzifyCentroid(NumSamples : int) -> float:
	# calculate the step size
	var StepSize : float = (maxRange - minRange)/NumSamples
	
	var TotalArea : float = 0.0
	var SumOfMoments : float = 0.0
	
	# step through the range of this variable in increments equal to StepSize
	# adding up the contribution (lower of CalculateDOM or the actual DOM of this
	# variable's fuzzified value) for each subset. This gives an approximation of
	# the total area of the fuzzy manifold.(This is similar to how the area under
	# a curve is calculated using calculus... the heights of lots of 'slices' are
	# summed to give the total area.)
	#
	# in addition the moment of each slice is calculated and summed. Dividing
	# the total area by the sum of the moments gives the centroid. (Just like
	# calculating the center of mass of an object)
	for samp in range(1, NumSamples + 1, 1):
		# for each set get the contribution to the area. This is the lower of the 
		# value returned from CalculateDOM or the actual DOM of the fuzzified 
		# value itself   
		for curSet in memberSets.keys():
			var contribution : float = min(memberSets[curSet].CalculateDOM(minRange + samp * StepSize), memberSets[curSet].GetDOM())
			
			TotalArea += contribution
			
			SumOfMoments += (minRange + samp * StepSize)  * contribution
	
	# make sure total area is not equal to zero
	if Utils.isEqual(0, TotalArea):
		return 0.0
	
	return (SumOfMoments / TotalArea)
