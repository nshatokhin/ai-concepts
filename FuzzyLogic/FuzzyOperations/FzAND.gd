extends FuzzyTerm
class_name FzAND

#-----------------------------------------------------------------------------
#   Class to provide the fuzzy AND operator to be used in
#   the creation of a fuzzy rule base
#-----------------------------------------------------------------------------


# an instance of this class may AND together up to 4 terms
var terms : Array = [] # std::vector<FuzzyTerm*>

# disallow assignment
# TODO
# FzAND& operator=(const FzAND&);

#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		for curTerm in terms:
#			curTerm.free()

# copy ctor
func CopyFrom(fa : FzAND) -> void:
	for curTerm in fa.terms:
		terms.push_back(curTerm.Clone())

# ctors accepting fuzzy terms.
#func _init(op1 : FuzzyTerm, op2 : FuzzyTerm):
#	terms.push_back(op1.Clone())
#	terms.push_back(op2.Clone())
#
#
#func _init(op1 : FuzzyTerm, op2 : FuzzyTerm, op3 : FuzzyTerm):
#	terms.push_back(op1.Clone())
#	terms.push_back(op2.Clone())
#	terms.push_back(op3.Clone())


func _init(op1 : FuzzyTerm = null, op2 : FuzzyTerm = null, op3 : FuzzyTerm = null, op4 : FuzzyTerm  = null):
	if op1:
		terms.push_back(op1.Clone())
	if op2:
		terms.push_back(op2.Clone())
	if op3:
		terms.push_back(op3.Clone())
	if op4:
		terms.push_back(op4.Clone())

# virtual ctor
func Clone() -> FuzzyTerm:
	var copy = get_script().new()
	copy.CopyFrom(self)
	return copy

# the AND operator returns the minimum DOM of the sets it is operating on
func GetDOM() -> float:
	var smallest : float = Constants.MaxFloat
	
	for curTerm in terms:
		if curTerm.GetDOM() < smallest:
			smallest = curTerm.GetDOM()
	
	return smallest


func ClearDOM() -> void:
	for curTerm in terms:
		curTerm.ClearDOM()
		

func ORwithDOM(val : float) -> void:
	for curTerm in terms:
		curTerm.ORwithDOM(val)
