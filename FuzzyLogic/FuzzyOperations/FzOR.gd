extends FuzzyTerm
class_name FzOR

#-----------------------------------------------------------------------------
#   Class to provide the fuzzy OR operator to be used in
#   the creation of a fuzzy rule base
#-----------------------------------------------------------------------------

# an instance of this class may AND together up to 4 terms
var terms : Array = [] # std::vector<FuzzyTerm*>

# no assignment op necessary
# TODO
# FzOR& operator=(const FzOR&);

# virtual ctor
func Clone() -> FuzzyTerm:
	return FzOR.new(self)

# unused
func ClearDOM() -> void:
	print("<FzOR::ClearDOM>: invalid context")
	
func ORwithDOM(val : float) -> void:
	print("<FzOR::ORwithDOM>: invalid context")

func _init(fa : FzOR):
	for curTerm in fa.terms:
		terms.push_back(curTerm.Clone())
   
# ctor using two terms
func _init(op1 : FuzzyTerm, op2 : FuzzyTerm):
	terms.push_back(op1.Clone())
	terms.push_back(op2.Clone())


# ctor using three terms
func _init(op1 : FuzzyTerm, op2 : FuzzyTerm, op3 : FuzzyTerm):
	terms.push_back(op1.Clone())
	terms.push_back(op2.Clone())
	terms.push_back(op3.Clone())


# ctor using four terms
func _init(op1 : FuzzyTerm, op2 : FuzzyTerm, op3 : FuzzyTerm, op4 : FuzzyTerm):
	terms.push_back(op1.Clone())
	terms.push_back(op2.Clone())
	terms.push_back(op3.Clone())
	terms.push_back(op4.Clone())


#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		for curTerm in terms:
#			curTerm.free()
  

# the OR operator returns the maximum DOM of the sets it is operating on
func GetDOM() -> float:
	var largest : float = Constants.MinFloat
	
	for curTerm in terms:
		if curTerm.GetDOM() > largest:
			largest = curTerm.GetDOM()
	
	return largest
