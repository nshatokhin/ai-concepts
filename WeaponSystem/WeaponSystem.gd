extends Reference
class_name WeaponSystem

#-----------------------------------------------------------------------------
#
#   Class to manage all operations specific to weapons and their deployment
#
#-----------------------------------------------------------------------------

var agent

# pointers to the weapons the bot is carrying (a bot may only carry one
# instance of each weapon)
var weaponMap : Dictionary = {}

# a pointer to the weapon the bot is currently holding
var currentWeapon

# this is the minimum amount of time a bot needs to see an opponent before
# it can react to it. This variable is used to prevent a bot shooting at
# an opponent the instant it becomes visible.
var reactionTime : float

# each time the current weapon is fired a certain amount of random noise is
# added to the the angle of the shot. This prevents the bots from hitting
# their opponents 100% of the time. The lower this value the more accurate
# a bot's aim will be. Recommended values are between 0 and 0.2 (the value
# represents the max deviation in radians that can be added to each shot).
var aimAccuracy : float

# the amount of time a bot will continue aiming at the position of the target
# even if the target disappears from view.
var aimPersistance : float

var rng : RandomNumberGenerator = RandomNumberGenerator.new()

const BlasterWeapon : PackedScene = preload("res://Weapons/Blaster/Blaster.tscn")
const ShotgunWeapon : PackedScene = preload("res://Weapons/Shotgun/Shotgun.tscn")
const RailGunWeapon : PackedScene = preload("res://Weapons/RailGun/RailGun.tscn")
const RocketLauncherWeapon : PackedScene = preload("res://Weapons/RocketLauncher/RocketLauncher.tscn")


#------------------------- ctor ----------------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, var ReactionTime : float, var AimAccuracy : float, var AimPersistance : float):
	rng.randomize()
	agent = bot_agent
	reactionTime = ReactionTime
	aimAccuracy = AimAccuracy
	aimPersistance = AimPersistance
	Initialize()

#------------------------- dtor ----------------------------------------------
#-----------------------------------------------------------------------------
#func _notification(what : int) -> void:
#	if what == NOTIFICATION_PREDELETE:
#		for weapon in weaponMap.keys():
#			weaponMap[weapon] = null
#		weaponMap.clear()
		

#------------------------------ Initialize -----------------------------------
#
#  initializes the weapons
#-----------------------------------------------------------------------------
func Initialize() -> void:
	# delete any existing weapons
	for weapon in weaponMap.keys():
		if weaponMap[weapon]:
			weaponMap[weapon] = null
	weaponMap.clear()
	
	# set up the container
	currentWeapon = BlasterWeapon.instance()
	currentWeapon.init(agent)
	
	weaponMap[Weapon.WeaponType.Blaster]			= currentWeapon
	weaponMap[Weapon.WeaponType.Shotgun]			= 0
	weaponMap[Weapon.WeaponType.RailGun]			= 0
	weaponMap[Weapon.WeaponType.RocketLauncher]		= 0
	
	agent.weaponNode.add_child(currentWeapon)


#-------------------------------- SelectWeapon -------------------------------
#
#-----------------------------------------------------------------------------
func SelectWeapon() -> void:
	# if a target is present use fuzzy logic to determine the most desirable 
	# weapon.
	var weapon
	
	if agent.targetSystem.isTargetPresent():
		# calculate the distance to the target
		var DistToTarget : float = (agent.global_position - agent.targetSystem.currentTarget.global_position).length()
		
		# for each weapon in the inventory calculate its desirability given the 
		# current situation. The most desirable weapon is selected
		var BestSoFar : float = Constants.MinFloat
		
		for curWeap in weaponMap.keys():
			# grab the desirability of this weapon (desirability is based upon
			# distance to target and ammo remaining)
			if weaponMap[curWeap]:
				var score : float = weaponMap[curWeap].GetDesirability(DistToTarget)
				
				# if it is the most desirable so far select it
				if score > BestSoFar:
					BestSoFar = score
					
					# place the weapon in the bot's hand.
					weapon = weaponMap[curWeap]
	else:
		weapon = weaponMap[Weapon.WeaponType.Blaster]
	
	SetWeapon(weapon)

func SetWeapon(weapon) -> void:
	if currentWeapon != weapon:
		agent.weaponNode.remove_child(currentWeapon)
		currentWeapon = weapon
		agent.weaponNode.add_child(currentWeapon)
#--------------------  AddWeapon ------------------------------------------
#
#   this is called by a weapon affector and will add a weapon of the specified
#   type to the bot's inventory.
#
#   if the bot already has a weapon of this type then only the ammo is added
#-----------------------------------------------------------------------------
func AddWeapon(weapon_type : int) -> void:
	# create an instance of this weapon
	var w : Weapon = null
	
	match weapon_type:
		Weapon.WeaponType.RailGun:
			w = RailGunWeapon.instance()
		
		Weapon.WeaponType.Shotgun:
			w = ShotgunWeapon.instance()
		
		Weapon.WeaponType.RocketLauncher:
			w = RocketLauncherWeapon.instance()
	
	if w:
		w.init(agent)
	else:
		return
	
	# if the bot already holds a weapon of this type, just add its ammo
	var present : = GetWeaponFromInventory(weapon_type)
	
	if present:
		present.IncrementRounds(w.NumRoundsRemaining())
		#w.free()
	# if not already holding, add to inventory
	else:
		weaponMap[weapon_type] = w


#------------------------- GetWeaponFromInventory -------------------------------
#
#   returns a pointer to any matching weapon.
#
#   returns a null pointer if the weapon is not present
#-----------------------------------------------------------------------------
func GetWeaponFromInventory(weapon_type : int) -> Weapon:
	return weaponMap[weapon_type]

#----------------------- ChangeWeapon ----------------------------------------
func ChangeWeapon(type : int) -> void:
	var w : Weapon = GetWeaponFromInventory(type)
	
	if w:
		agent.weaponNode.remove_child(currentWeapon)
		currentWeapon = w
		agent.weaponNode.add_child(currentWeapon)


#--------------------------- TakeAimAndShoot ---------------------------------
#
#   this method aims the bots current weapon at the target (if there is a
#   target) and, if aimed correctly, fires a round
#-----------------------------------------------------------------------------
func TakeAimAndShoot() -> void:
	# aim the weapon only if the current target is shootable or if it has only
	# very recently gone out of view (this latter condition is to ensure the 
	# weapon is aimed at the target even if it temporarily dodges behind a wall
	# or other cover)
	if agent.targetSystem.isTargetShootable() or agent.targetSystem.GetTimeTargetHasBeenOutOfView() < aimPersistance:
		# the position the weapon will be aimed at
		var targetBot = agent.GetTargetBot()
		var AimingPos : Vector2 = targetBot.global_position
		
		# if the current weapon is not an instant hit type gun the target position
		# must be adjusted to take into account the predicted movement of the 
		# target
		if currentWeapon.type == Weapon.WeaponType.RocketLauncher or currentWeapon.type == Weapon.WeaponType.Blaster:
			AimingPos = PredictFuturePositionOfTarget()
			
			# if the weapon is aimed correctly, there is line of sight between the
			# bot and the aiming position and it has been in view for a period longer
			# than the bot's reaction time, shoot the weapon
			if agent.RotateFacingTowardPosition(AimingPos) and agent.targetSystem.GetTimeTargetHasBeenVisible() > reactionTime and agent.hasLOSto(AimingPos, targetBot):
				#AddNoiseToAim(AimingPos)
				currentWeapon.ShootAt(AimingPos)
		# no need to predict movement, aim directly at target
		else:
			# if the weapon is aimed correctly and it has been in view for a period
			# longer than the bot's reaction time, shoot the weapon
			if agent.RotateFacingTowardPosition(AimingPos) and agent.targetSystem.GetTimeTargetHasBeenVisible() > reactionTime:
				AddNoiseToAim(AimingPos)
				currentWeapon.ShootAt(AimingPos)

	# no target to shoot at so rotate facing to be parallel with the bot's
	# heading direction
	else:
		agent.RotateFacingTowardPosition(agent.global_position + agent.heading)


#---------------------------- AddNoiseToAim ----------------------------------
#
#   adds a random deviation to the firing angle not greater than m_dAimAccuracy 
#   rads
#-----------------------------------------------------------------------------
func AddNoiseToAim(AimingPos : Vector2) -> void:
	var toPos : Vector2 = AimingPos - agent.global_position
	
	# create a transformation matrix
	var mat : Transform2D = Transform2D()
	# rotate
	mat = mat.rotated(rng.randf_range(-aimAccuracy, aimAccuracy))
	# now transform
	toPos = mat.xform(toPos)
	
	AimingPos = toPos + agent.global_position


#-------------------------- PredictFuturePositionOfTarget --------------------
#
#   predicts where the target will be located in the time it takes for a
#   projectile to reach it. This uses a similar logic to the Pursuit steering
#   behavior.
#-----------------------------------------------------------------------------
func PredictFuturePositionOfTarget() -> Vector2:
	var MaxSpeed : float = currentWeapon.maxProjectileSpeed

	# if the target is ahead and facing the agent shoot at its current pos
	var ToEnemy : Vector2 = agent.GetTargetBot().global_position - agent.global_position
	
	# the lookahead time is proportional to the distance between the enemy
	# and the pursuer; and is inversely proportional to the sum of the
	# agent's velocities
	var LookAheadTime : float = ToEnemy.length() / (MaxSpeed + agent.GetTargetBot().maxSpeed)
	# return the predicted future position of the enemy
	return agent.GetTargetBot().global_position + agent.GetTargetBot().velocity * LookAheadTime


#------------------ GetAmmoRemainingForWeapon --------------------------------
#
#   returns the amount of ammo remaining for the specified weapon. Return zero
#   if the weapon is not present
#-----------------------------------------------------------------------------
func GetAmmoRemainingForWeapon(weapon_type : int) -> int:
	if weaponMap[weapon_type]:
		return weaponMap[weapon_type].NumRoundsRemaining()
	
	return 0


#---------------------------- ShootAt ----------------------------------------
#
#   shoots the current weapon at the given position
#-----------------------------------------------------------------------------
func ShootAt(pos : Vector2) -> void:
	currentWeapon.ShootAt(pos)
	

func GetDesirabilitiesText() -> String:
	var text : String = ""
	
	for curWeap in weaponMap.keys():
		if weaponMap[curWeap]:
			var score : float = weaponMap[curWeap].GetLastDesirabilityScore()
			var type : String = Weapon.GetNameOfType(weaponMap[curWeap].type)
			
			text += String(score) + " " + type + "\n"
	
	return text
