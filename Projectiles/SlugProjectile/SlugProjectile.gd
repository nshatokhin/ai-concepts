extends Node2D
class_name SlugProjectile

const Params : = preload("res://Projectiles/SlugProjectile/SlugProjectile.tres")

onready var trasser_line : Line2D = $Trasser
onready var timer : Timer = $Timer

var origin : Vector2
var target : Vector2
#-------------------------- ctor ---------------------------------------------
#-----------------------------------------------------------------------------
func Init(shooter, Target : Vector2, Origin : Vector2) -> void:
#	.BaseInit(target, shooter, origin, shooter.facing, Params.Damage, 0, Params.Mass, Params.MaxForce)
	if target == Vector2():
		print("target to self", shooter)
	
	origin = Origin
	target = Target
	
func _ready():
	trasser_line.points[0] = origin
	trasser_line.points[1] = target
	
	timer.start(Params.LifeTime)


func _on_Timer_timeout():
	queue_free()
