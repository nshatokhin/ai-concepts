extends Projectile
class_name RocketProjectile

const Params : = preload("res://Projectiles/RocketProjectile/RocketProjectile.tres")

#-------------------------- ctor ---------------------------------------------
#-----------------------------------------------------------------------------
func Init(shooter, target : Vector2, speed : float) -> void:
	.BaseInit(target, shooter, Vector2(), shooter.facing, Params.Damage, speed, Params.Mass, Params.MaxForce)
	if target == Vector2():
		print("target to self", shooter)
		
	add_collision_exception_with(shooter)
	
	linear_velocity = heading * maxSpeed
	

func _on_BoltProjectile_body_entered(body):
	print("Hit: ", body.name)
	AddExplosion()
	queue_free()


func AddExplosion():
	get_node("/root").add_child(TriggerExplosion.new(shooter, global_position, Params.ExplosionRadius, damageInflicted))
