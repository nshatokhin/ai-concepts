extends RigidBody2D
class_name Projectile

#-----------------------------------------------------------------------------
#     Base class to define a projectile type. A projectile of the correct
#     type is created whnever a weapon is fired. In Raven there are four
#     types of projectile: Slugs (railgun), Pellets (shotgun), Rockets
#     (rocket launcher ) and Bolts (Blaster) 
#-----------------------------------------------------------------------------

# the entity that fired this
var shooter

# the place the projectile is aimed at
var target : Vector2

# where the projectile was fired from
var origin : Vector2

# how much damage the projectile inflicts
var damageInflicted : int

# is it dead? A dead projectile is one that has come to the end of its
# trajectory and cycled through any explosion sequence. A dead projectile
# can be removed from the world environment and deleted.
var dead : bool

# this is set to true as soon as a projectile hits something
var impacted : bool

# the position where this projectile impacts an object
var impactPoint : Vector2

# this is stamped with the time this projectile was instantiated. This is
# to enable the shot to be rendered for a specific length of time
var timeOfCreation : float

var maxSpeed : float
var maxForce : float

var heading : Vector2

func GetClosestIntersectingBot(From : Vector2, To : Vector2):
	pass

func GetListOfIntersectingBots(From : Vector2, To : Vector2) -> Array:
	return []


func BaseInit(Target : Vector2, Shooter, Origin : Vector2, Heading : Vector2, damage : int, MaxSpeed : float, Mass : float, MaxForce : float):
	maxSpeed = MaxSpeed
	maxForce = MaxForce
	mass = Mass
	rotation = Heading.angle()
	target = Target
	dead = false
	impacted = false
	damageInflicted = damage
	origin = Origin
	shooter = Shooter
	timeOfCreation = Clock.GetCurrentTime()
	
	heading = Heading
	
	# linear_velocity = heading * maxSpeed
	global_position = Origin

# unimportant for this class unless you want to implement a full state 
# save/restore (which can be useful for debugging purposes)
#void Write(std::ostream&  os)const{}
#void Read (std::ifstream& is){}

# set to true if the projectile has impacted and has finished any explosion 
# sequence. When true the projectile will be removed from the game
func isDead() -> bool:
	return dead

# true if the projectile has impacted but is not yet dead (because it
# may be exploding outwards from the point of impact for example)
func HasImpacted() -> bool:
	return impacted
