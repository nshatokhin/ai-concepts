extends Projectile
class_name ShellProjectile

const Params : = preload("res://Projectiles/ShellProjectile/ShellProjectile.tres")

#-------------------------- ctor ---------------------------------------------
#-----------------------------------------------------------------------------
func Init(shooter, target : Vector2, Origin: Vector2, speed : float) -> void:
	.BaseInit(target, shooter, Origin, shooter.facing, Params.Damage, speed, Params.Mass, Params.MaxForce)
	if target == Vector2():
		print("target to self", shooter)
	
	add_collision_exception_with(shooter)
	#apply_impulse(Vector2(0, 0), heading * maxSpeed)
	linear_velocity = (target - Origin).normalized() * maxSpeed
	
	
#------------------------------ Update ---------------------------------------
#-----------------------------------------------------------------------------
#func _physics_process(delta):
#{
#	if (!m_bImpacted)
#	{
#	m_vVelocity = MaxSpeed() * Heading();
#	
#	//make sure vehicle does not exceed maximum velocity
#	m_vVelocity.Truncate(m_dMaxSpeed);
#	
#	//update the position
#	m_vPosition += m_vVelocity;
#	
#	
#	//if the projectile has reached the target position or it hits an entity
#	//or wall it should explode/inflict damage/whatever and then mark itself
#	//as dead
#	
#	
#	//test to see if the line segment connecting the bolt's current position
#	//and previous position intersects with any bots.
#	Raven_Bot* hit = GetClosestIntersectingBot(m_vPosition - m_vVelocity,
#	m_vPosition);
#	
#	//if hit
#	if (hit)
#	{
#	m_bImpacted = true;
#	m_bDead     = true;
#	
#	//send a message to the bot to let it know it's been hit, and who the
#	//shot came from
#	Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
#	m_iShooterID,
#	hit->ID(),
#	Msg_TakeThatMF,
#	(void*)&m_iDamageInflicted);
#	}
#	
#	//test for impact with a wall
#	double dist;
#	if( FindClosestPointOfIntersectionWithWalls(m_vPosition - m_vVelocity,
#	m_vPosition,
#	dist,
#	m_vImpactPoint,
#	m_pWorld->GetMap()->GetWalls()))
#	{
#	m_bDead     = true;
#	m_bImpacted = true;
#	
#	m_vPosition = m_vImpactPoint;
#	
#	return;
#	}
#	}
#}


func _on_BoltProjectile_body_entered(body):
	print("Hit: ", body.name)
	if body != shooter:
		var damage_func : FuncRef = funcref(body, "add_damage")
	
		if damage_func.is_valid():
			damage_func.call_func(damageInflicted, shooter)
			
		queue_free()
