extends Reference
class_name SensoryMemory

# the owner of this instance
var agent

# this container is used to simulate memory of sensory events. A MemoryRecord
# is created for each opponent in the environment. Each record is updated 
# whenever the opponent is encountered. (when it is seen or heard)
var memoryMap : Dictionary = {}

# a bot has a memory span equivalent to this value. When a bot requests a 
# list of all recently sensed opponents this value is used to determine if 
# the bot is able to remember an opponent or not.
var memorySpan : float

#------------------------------- ctor ----------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, MemorySpan : float):
	agent = bot_agent
	memorySpan = MemorySpan

#--------------------- MakeNewRecordIfNotAlreadyPresent ----------------------

func MakeNewRecordIfNotAlreadyPresent(opponent) -> void:
	# else check to see if this Opponent already exists in the memory. If it doesn't,
	# create a new record
	if not memoryMap.has(opponent):
		memoryMap[opponent] = MemoryRecord.new()

#------------------------ RemoveBotFromMemory --------------------------------
#
#  this removes a bot's record from memory
#-----------------------------------------------------------------------------
func RemoveBotFromMemory(bot) -> void:
	if memoryMap.has(bot):
		memoryMap.erase(memoryMap[bot])
  
#----------------------- UpdateWithSoundSource -------------------------------
#
# this updates the record for an individual opponent. Note, there is no need to
# test if the opponent is within the FOV because that test will be done when the
# UpdateVision method is called
#-----------------------------------------------------------------------------
func UpdateWithSoundSource(noiseMaker) -> void:
	# make sure the bot being examined is not this bot
	if agent != noiseMaker:
		# if the bot is already part of the memory then update its data, else
		# create a new memory record and add it to the memory
		MakeNewRecordIfNotAlreadyPresent(noiseMaker)
		
		var info : MemoryRecord = memoryMap[noiseMaker]
		
		# test if there is LOS between bots 
		if agent.hasLOSto(noiseMaker.global_position, noiseMaker):
			info.shootable = true
			
			# record the position of the bot
			info.lastSensedPosition = noiseMaker.global_position
		else:
			info.shootable = false
			info.lastSensedPosition = noiseMaker.global_position
		
		# record the time it was sensed
		info.timeLastSensed = OS.get_system_time_msecs() / 1000.0

#----------------------------- UpdateVision ----------------------------------
#
#  this method iterates through all the bots in the game world to test if
#  they are in the field of view. Each bot's memory record is updated
#  accordingly
#-----------------------------------------------------------------------------
func UpdateVision() -> void:
	# for each bot in the world test to see if it is visible to the owner of
	# this class
	var bots : Array = agent.neighbors()#WorldController.GetAllBots()

	for curBot in bots:
		# make sure the bot being examined is not this bot
		if agent != curBot:
			# make sure it is part of the memory map
			MakeNewRecordIfNotAlreadyPresent(curBot)
			
			# get a reference to this bot's data
			var info : MemoryRecord = memoryMap[curBot]
			
			# test if there is LOS between bots 
			if agent.hasLOStoBot(curBot):
				info.shootable = true
				
				# test if the bot is within FOV
				if isSecondInFOVOfFirst(agent.global_position,  agent.facing, curBot.global_position, agent.fieldOfView):
					info.timeLastSensed = OS.get_system_time_msecs() / 1000.0
					info.lastSensedPosition = curBot.global_position
					info.timeLastVisible = OS.get_system_time_msecs() / 1000.0
					
					if info.withinFOV == false:
						info.withinFOV = true
						info.timeBecameVisible = info.timeLastSensed
				else:
					info.withinFOV = false
			else:
				info.shootable = false
				info.withinFOV = false
			
			
	#next bot


#------------------------ GetListOfRecentlySensedOpponents -------------------
#
#  returns a list of the bots that have been sensed recently
#-----------------------------------------------------------------------------
func GetListOfRecentlySensedOpponents() -> Array:
	# this will store all the opponents the bot can remember
	var opponents : Array = []
	
	var CurrentTime : float = OS.get_system_time_msecs() / 1000.0
	
	for curRecord in memoryMap.keys():
		# if this bot has been updated in the memory recently, add to list
		if (CurrentTime - memoryMap[curRecord].timeLastSensed) <= memorySpan:
			opponents.push_back(curRecord)
	
	return opponents

#----------------------------- isOpponentShootable --------------------------------
#
#  returns true if the bot given as a parameter can be shot (ie. its not
#  obscured by walls)
#-----------------------------------------------------------------------------
func isOpponentShootable(opponent) -> bool:
	if memoryMap.has(opponent):
		return memoryMap[opponent].shootable
	
	return false

#----------------------------- isOpponentWithinFOV --------------------------------
#
#  returns true if the bot given as a parameter is within FOV
#-----------------------------------------------------------------------------
func isOpponentWithinFOV(opponent) -> bool:
	if memoryMap.has(opponent):
		return memoryMap[opponent].withinFOV

	return false

#---------------------------- GetLastRecordedPositionOfOpponent -------------------
#
#  returns the last recorded position of the bot
#-----------------------------------------------------------------------------
func GetLastRecordedPositionOfOpponent(opponent) -> Vector2:
	if memoryMap.has(opponent):
		return memoryMap[opponent].lastSensedPosition
		
	print("<SensoryMemory::GetLastRecordedPositionOfOpponent>: Attempting to get position of unrecorded bot")
	return Vector2()

#----------------------------- GetTimeOpponentHasBeenVisible ----------------------
#
#  returns the amount of time the given bot has been visible
#-----------------------------------------------------------------------------
func GetTimeOpponentHasBeenVisible(opponent) -> float:
	if memoryMap.has(opponent) and memoryMap[opponent].withinFOV:
		return OS.get_system_time_msecs() / 1000.0 - memoryMap[opponent].timeBecameVisible;
		
	return 0.0


#-------------------- GetTimeOpponentHasBeenOutOfView ------------------------
#
#  returns the amount of time the given opponent has remained out of view
#  returns a high value if opponent has never been seen or not present
#-----------------------------------------------------------------------------
func GetTimeOpponentHasBeenOutOfView(opponent) -> float:
	if memoryMap.has(opponent):
		return OS.get_system_time_msecs() / 1000.0 - memoryMap[opponent].timeLastVisible

	return Constants.MaxFloat

#------------------------ GetTimeSinceLastSensed ----------------------
#
#  returns the amount of time the given bot has been visible
#-----------------------------------------------------------------------------
func GetTimeSinceLastSensed(opponent) -> float:
	if memoryMap.has(opponent) and memoryMap[opponent].withinFOV:
		return OS.get_system_time_msecs() / 1000.0 - memoryMap[opponent].timeLastSensed
	
	return 0.0

#------------------ isSecondInFOVOfFirst -------------------------------------
#
#  returns true if the target position is in the field of view of the entity
#  positioned at posFirst facing in facingFirst
#-----------------------------------------------------------------------------
func isSecondInFOVOfFirst(posFirst : Vector2, facingFirst : Vector2, posSecond : Vector2, fov : float) -> bool:
	var toTarget : Vector2 = (posSecond - posFirst).normalized()
	
	return facingFirst.dot(toTarget) >= cos(fov/2.0)
