extends Node2D
class_name GameMap

var points : Dictionary = {}
var points_to_add : Array = []

var edges : Dictionary = {}


var max_generation_x = 12
var max_generation_y = 7
var points_distance = 50
var point_radius = 5
var max_cycles_per_update = 60

var graph_filling

var graph : SparseGraph

onready var fps_label = get_node("UI/FPSLabel")

func _ready():
	init_filling(Vector2(-100, 0))
	fill_points()
	create_graph()
	
	WorldController.cellSpaceNeighborhoodRange = GraphUtils.CalculateAverageGraphEdgeLength(graph)
	WorldController.graph = graph
	
	$BotSpawner.SpawnBot(PoolVector2Array([Vector2(-100, 0), Vector2(-300, 0), Vector2(-100, -100)]))
	$BotSpawner2.SpawnBot()
#	$BotSpawner3.SpawnBot()
  
func create_graph():
	graph = SparseGraph.new(false)
	
	for key in points.keys():
		points[key].index = graph.AddNode(NavGraphNode.new(graph.GetNextFreeNodeIndex(), points[key].position))
	
	for key in edges.keys():
		graph.AddEdge(NavGraphEdge.new(points[point_hash(edges[key].from)].index, points[point_hash(edges[key].to)].index, 1.0))
	
	WorldController.triggers = get_tree().get_nodes_in_group("Items")
	for item in WorldController.triggers:
		var closest_node : = GetClosestNodeToPosition(item.global_position)
		item.graphNodeIndex = graph.AddNode(NavGraphNode.new(graph.GetNextFreeNodeIndex(), item.global_position, item))
		
		if closest_node != PathPlanner.NoClosestNodeFound:
			graph.AddEdge(NavGraphEdge.new(item.graphNodeIndex, closest_node, 1.0))


func _physics_process(delta):
	if Input.is_action_just_pressed("ExorciseAllBots"):
		ExorciseAnyPossessedBot()
		
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

	if WorldController.selectedBot:
		if Input.is_action_just_pressed("SelectBlaster"):
			WorldController.selectedBot.SetWeapon(Weapon.WeaponType.Blaster)

		if Input.is_action_just_pressed("SelectShotgun"):
			WorldController.selectedBot.SetWeapon(Weapon.WeaponType.Shotgun)
			
		if Input.is_action_just_pressed("SelectRailGun"):
			WorldController.selectedBot.SetWeapon(Weapon.WeaponType.RailGun)

		if Input.is_action_just_pressed("SelectRocketLauncher"):
			WorldController.selectedBot.SetWeapon(Weapon.WeaponType.RocketLauncher)
		
	fps_label.set_text(str(Engine.get_frames_per_second()))
#	var space_state = get_world_2d().direct_space_state
#  
#	var counter = 0
#	while not points_to_add.empty() and counter < max_cycles_per_update:
#		cycle_once(space_state)
#		counter += 1
#
#	update()

func init_filling(start_position : Vector2):
	points_to_add.clear()
	points_to_add.push_back({"position": start_position, "x": 0, "y": 0})
  
func fill_points():
	var space_state = get_world_2d().direct_space_state
	while not points_to_add.empty():
		cycle_once(space_state)

func cycle_once(space_state) -> void:
	var point = points_to_add.pop_front()

	var ss = get_world_2d().get_direct_space_state()
	var shape = CircleShape2D.new()
	shape.set_radius(points_distance/2)
	var query = Physics2DShapeQueryParameters.new()
	query.set_exclude([])
	query.set_shape(shape)
	query.set_transform(Transform2D(0.0, point.position))
	var res = ss.intersect_shape(query)
	
	if (res.size() > 0):
		return

	if "from" in point:
		var result = space_state.intersect_ray(point.from, point.position)
  
		if not result.empty() and result.collider.is_in_group("Obstacles"):
			if "from" in point:
				add_edge(point.from, result.position)
				
			points[point_hash(result.position)] = {"index": 0, "position": result.position, "x": point.x, "y": point.y}
			return
  
	if point_already_exists(point.position):
		add_edge(point.from, point.position)
		return
	
	if point.x >= max_generation_x or point.y >= max_generation_y or point.x <= -max_generation_x or point.y <= -max_generation_y:
		return

	if "from" in point:
		add_edge(point.from, point.position)
		
	points[point_hash(point.position)] = {"index": 0, "position": point.position, "x": point.x, "y": point.y}

	var pos = point.position + Vector2(1, 0) * points_distance
	if not point_already_exists(pos):
		points_to_add.push_back({"position": pos, "x": point.x + 1, "y": point.y, "from": point.position})
	pos = point.position + Vector2(-1, 0) * points_distance
	if not point_already_exists(pos):
		points_to_add.push_back({"position": pos, "x": point.x - 1, "y": point.y, "from": point.position})
	pos = point.position + Vector2(0, 1) * points_distance
	if not point_already_exists(pos):
		points_to_add.push_back({"position": pos, "x": point.x, "y": point.y + 1, "from": point.position})
	pos = point.position + Vector2(0, -1) * points_distance
	if not point_already_exists(pos):
		points_to_add.push_back({"position": pos, "x": point.x, "y": point.y - 1, "from": point.position})


func point_already_exists(position : Vector2) -> bool:
	return points.has(point_hash(position))

func point_hash(position : Vector2) -> String:
	return "x" + String(position.x) + "y" + String(position.y)

func add_edge(from : Vector2, to : Vector2) -> void:
	if edge_already_exists(from, to) or edge_already_exists(to, from):
		return
	
	edges[edge_hash(from, to)] = {"from": from, "to": to}
  
func edge_already_exists(from : Vector2, to : Vector2) -> bool:
	return edges.has(edge_hash(from, to))

func edge_hash(from : Vector2, to : Vector2) -> String:
	return point_hash(from) + "-" + point_hash(to)
  
func _draw():
	if UserOptions.DebugDraw:
#	for key in points.keys():
#		draw_circle(points[key].position, point_radius, Color("#ffff0000"))
		
#	for key in edges.keys():
#		draw_line(edges[key].from, edges[key].to, Color("#ff00ff00"))
		graph.draw(self)

func _unhandled_input(event):
	if event is InputEventMouseButton:
		var mouse_position = get_global_mouse_position()
			
		if event.button_index == BUTTON_LEFT:
			ClickLeftMouseButton(mouse_position)
		elif event.button_index == BUTTON_RIGHT:
			ClickRightMouseButton(mouse_position)
			
	if event is InputEventMouseMotion:
		var mouse_position = get_global_mouse_position()
		
		if WorldController.selectedBot and WorldController.selectedBot.isPossessed():
			WorldController.selectedBot.RotateFacingTowardPosition(mouse_position)
			
#------------------------- ExorciseAnyPossessedBot ---------------------------
#   when called will release any possessed bot from user control
#-----------------------------------------------------------------------------
func ExorciseAnyPossessedBot() -> void:
	if WorldController.selectedBot:
		WorldController.selectedBot.Exorcise()


#-------------------------- ClickRightMouseButton -----------------------------
#
#   this method is called when the user clicks the right mouse button.
#
#   the method checks to see if a bot is beneath the cursor. If so, the bot
#   is recorded as selected.
#
#   if the cursor is not over a bot then any selected bot/s will attempt to
#   move to that position.
#-----------------------------------------------------------------------------
func ClickRightMouseButton(pos : Vector2) -> void:
	var bot = null
	var space = get_world_2d().get_direct_space_state()
	var result = space.intersect_point( pos, 1)
			
	if not result.empty() and result[0].collider.is_in_group("Bots"):
		bot = result[0].collider

	# if there is no selected bot just return;
	if not bot and WorldController.selectedBot == null:
		return

	# if the cursor is over a different bot to the existing selection,
	# change selection
	if bot and bot != WorldController.selectedBot:
		if WorldController.selectedBot:
			WorldController.selectedBot.Exorcise()
		WorldController.selectedBot = bot
		return

	# if the user clicks on a selected bot twice it becomes possessed(under
	# the player's control)
	if bot and bot == WorldController.selectedBot:
		WorldController.selectedBot.TakePossession()
		
		# clear any current goals
		WorldController.selectedBot.brain.RemoveAllSubgoals()

	# if the bot is possessed then a right click moves the bot to the cursor
	# position
	if WorldController.selectedBot.isPossessed():
		# if the shift key is pressed down at the same time as clicking then the
		# movement command will be queued
		if Input.is_action_pressed("QueueCommand"):
			WorldController.selectedBot.brain.QueueGoal_MoveToPosition(pos)
		else:
			# clear any current goals
			WorldController.selectedBot.brain.RemoveAllSubgoals()
			
			WorldController.selectedBot.brain.AddGoal_MoveToPosition(pos)


#---------------------- ClickLeftMouseButton ---------------------------------
#-----------------------------------------------------------------------------
func ClickLeftMouseButton(pos : Vector2) -> void:
	if WorldController.selectedBot and WorldController.selectedBot.isPossessed():
		WorldController.selectedBot.FireWeapon(pos)


func GetClosestNodeToPosition(pos : Vector2) -> int:
	var ClosestSoFar : float = Constants.MaxFloat
	var ClosestNode : int = PathPlanner.NoClosestNodeFound
	
	# when the cell space is queried this the the range searched for neighboring
	# graph nodes. This value is inversely proportional to the density of a 
	# navigation graph (less dense = bigger values)
	#var rng : float = WorldController.cellSpaceNeighborhoodRange
	
	# calculate the graph nodes that are neighboring this position
	#WorldController.GetCellSpace().CalculateNeighbors(pos, rng)
	
	# iterate through the neighbors and sum up all the position vectors
	for node in graph.nodes:#WorldController.GetCellSpace():
		# if the path between this node and pos is unobstructed calculate the
		# distance
		if node.index == NavGraphNode.InvalidNodeIndex:
			continue
		
		var dist : float = (pos - node.position).length_squared()
		
		# keep a record of the closest so far
		if dist < ClosestSoFar:
			ClosestSoFar = dist;
			ClosestNode  = node.index

	return ClosestNode
