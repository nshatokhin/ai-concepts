extends Reference
class_name TargetingSystem

# the owner of this system
var agent = null

# the current target (this will be null if there is no target assigned)
var currentTarget = null

#-------------------------------- ctor ---------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent):
	agent = bot_agent
	currentTarget = null

#----------------------------- Update ----------------------------------------
#-----------------------------------------------------------------------------
func Update() -> void:
	var ClosestDistSoFar : float = Constants.MaxFloat
	currentTarget = null
	
	# grab a list of all the opponents the owner can sense
	var SensedBots : Array = []
	SensedBots = agent.sensoryMem.GetListOfRecentlySensedOpponents()
	
	for curBot in SensedBots:
		# make sure the bot is alive and that it is not the owner
		if curBot.isAlive() and curBot != agent:
			var dist : float = (curBot.global_position - agent.global_position).length_squared()
			
			if dist < ClosestDistSoFar:
				ClosestDistSoFar = dist
				currentTarget = curBot

# returns a pointer to the target. null if no target current.
func GetTarget():
	return currentTarget

# sets the target pointer to null
func ClearTarget() -> void:
	currentTarget = null

# returns true if the target is within the field of view of the owner
func isTargetWithinFOV() -> bool:
  return agent.sensoryMem.isOpponentWithinFOV(currentTarget)

# returns true if there is a currently assigned target
func isTargetPresent() -> bool:
	return currentTarget != null

# returns true if there is unobstructed line of sight between the target
# and the owner
func isTargetShootable() -> bool:
  return agent.sensoryMem.isOpponentShootable(currentTarget)

# returns the position the target was last seen. Throws an exception if
# there is no target currently assigned
func GetLastRecordedPosition() -> Vector2:
  return agent.sensoryMem.GetLastRecordedPositionOfOpponent(currentTarget)

# returns the amount of time the target has been in the field of view
func GetTimeTargetHasBeenVisible() -> float:
  return agent.sensoryMem.GetTimeOpponentHasBeenVisible(currentTarget)

# returns the amount of time the target has been out of view
func GetTimeTargetHasBeenOutOfView() -> float:
  return agent.sensoryMem.GetTimeOpponentHasBeenOutOfView(currentTarget)
