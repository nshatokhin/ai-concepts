extends Reference
class_name GraphSearchDijkstra

#----------------------- GraphSearchDijkstra --------------------------------
#
#  Given a graph, source and optional target this class solves for
#  single source shortest paths (without a target being specified) or 
#  shortest path from source to target.
#
#  The algorithm used is a priority queue implementation of Dijkstra's.
#  note how similar this is to the algorithm used in Graph_MinSpanningTree.
#  The main difference is in the calculation of the priority in the line:
#  
#  double NewCost = m_CostToThisNode[best] + pE->Cost;
#------------------------------------------------------------------------

var graph : SparseGraph

# this vector contains the edges that comprise the shortest path tree -
# a directed subtree of the graph that encapsulates the best paths from 
# every node on the SPT to the source node.
var shortestPathTree : Array = []

# this is indexed into by node index and holds the total cost of the best
# path found so far to the given node. For example, m_CostToThisNode[5]
# will hold the total cost of all the edges that comprise the best path
# to node 5, found so far in the search (if node 5 is present and has 
# been visited)
var costToThisNode : Array = [] 

# this is an indexed (by node) vector of 'parent' edges leading to nodes 
# connected to the SPT but that have not been added to the SPT yet. This is
# a little like the stack or queue used in BST and DST searches.
var searchFrontier : Array

var source : int
var target : int

func _init(g : SparseGraph, src : int, tgt : int = -1):
	graph = g
	shortestPathTree.resize(graph.NumNodes())
	searchFrontier.resize(graph.NumNodes())
	costToThisNode.resize(graph.NumNodes())
	source = src
	target = tgt
	
	for i in range(graph.NumNodes()):
		costToThisNode[i] = 0.0
	
	Search()
 
# returns the vector of edges that defines the SPT. If a target was given
# in the constructor then this will be an SPT comprising of all the nodes
# examined before the target was found, else it will contain all the nodes
# in the graph.
func GetSPT() -> Array:
	return shortestPathTree

# returns a vector of node indexes that comprise the shortest path
# from the source to the target. It calculates the path by working
# backwards through the SPT from the target node.
func GetPathToTarget() -> Array:
	var path : Array = []

	# just return an empty path if no target or no path found
	if target < 0:
		return path

	var nd : int = target

	path.push_front(nd)

	while nd != source and shortestPathTree[nd] != 0:
		nd = shortestPathTree[nd].from

		path.push_front(nd)

	return path

# returns the total cost to the target
func GetCostToTarget() -> float:
	return costToThisNode[target]

# returns the total cost to the given node
func GetCostToNode(nd : int) -> float:
	return costToThisNode[nd]

func Search() -> void:
	# create an indexed priority queue that sorts smallest to largest
	# (front to back).Note that the maximum number of elements the iPQ
	# may contain is N. This is because no node can be represented on the 
	# queue more than once.
	var pq : IndexedPriorityQLow = IndexedPriorityQLow.new(costToThisNode, graph.NumNodes())

	# put the source node on the queue
	pq.insert(source)

	# while the queue is not empty
	while not pq.empty():
		# get lowest cost node from the queue. Don't forget, the return value
		# is a *node index*, not the node itself. This node is the node not already
		# on the SPT that is the closest to the source node
		var NextClosestNode : int = pq.Pop()

		# move this edge from the frontier to the shortest path tree
		shortestPathTree[NextClosestNode] = searchFrontier[NextClosestNode]

		# if the target has been found exit
		if NextClosestNode == target:
			return

		# now to relax the edges.
		# for each edge connected to the next closest node
		var edges : Dictionary = graph.GetEdgesTo(NextClosestNode)
		for edge_key in edges.keys():
			# the total cost to the node this edge points to is the cost to the
			# current node plus the cost of the edge connecting them.
			var NewCost : float = costToThisNode[NextClosestNode] + edges[edge_key].cost

			# if this edge has never been on the frontier make a note of the cost
			# to get to the node it points to, then add the edge to the frontier
			# and the destination node to the PQ.
			if searchFrontier[edges[edge_key].to] == 0:
				costToThisNode[edges[edge_key].to] = NewCost

				pq.insert(edges[edge_key].to)

				searchFrontier[edges[edge_key].to] = edges[edge_key]

			# else test to see if the cost to reach the destination node via the
			# current node is cheaper than the cheapest cost found so far. If
			# this path is cheaper, we assign the new cost to the destination
			# node, update its entry in the PQ to reflect the change and add the
			# edge to the frontier
			elif NewCost < costToThisNode[edges[edge_key].to] and shortestPathTree[edges[edge_key].to] == 0:
				costToThisNode[edges[edge_key].to] = NewCost

				# because the cost is less than it was previously, the PQ must be
				# re-sorted to account for this.
				pq.ChangePriority(edges[edge_key].to)

				searchFrontier[edges[edge_key].to] = edges[edge_key]
