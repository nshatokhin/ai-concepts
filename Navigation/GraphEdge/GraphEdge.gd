extends Reference
class_name GraphEdge

enum {
	InvalidNodeIndex = -1
}

# An edge connects two nodes. Valid node indices are always positive.
var from : int
var to : int

# the cost of traversing the edge
var cost : float

# ctors
func _init(From : int = InvalidNodeIndex, To : int = InvalidNodeIndex, Cost : float = 1.0):
	cost = Cost
	from = From
	to = To
	
# these two operators are required
func equal(rhs : GraphEdge) -> bool:
	return rhs.from == from and rhs.to == to and rhs.cost == cost
