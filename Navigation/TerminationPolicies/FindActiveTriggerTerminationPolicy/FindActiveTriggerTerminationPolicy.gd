extends Reference
class_name FindActiveTriggerTerminationPolicy

# the search will terminate when the currently examined graph node
# is the same as the target node.

func isSatisfied(G : SparseGraph, target : int, CurrentNodeIdx : int) -> bool:
	var satisfied : bool = false

	# get a reference to the node at the given node index
	var node : NavGraphNode = G.GetNode(CurrentNodeIdx)

	# if the extrainfo field is pointing to a giver-trigger, test to make sure 
	# it is active and that it is of the correct type.
	if node.extraInfo != null and node.extraInfo.isActive() and node.extraInfo.itemType == target:
		satisfied = true
	
	return satisfied
