extends Reference
class_name FindNodeIndexTerminationPolicy

# hee search will terminate when the currently examined graph node
# is the same as the target node.

func isSatisfied(G : SparseGraph, target : int, CurrentNodeIdx : int) -> bool:
    return CurrentNodeIdx == target
