extends Reference
class_name PathManager

#-----------------------------------------------------------------------------
#   Class to manage a number of graph searches, and to 
#   distribute the calculation of each search over several update-steps
#-----------------------------------------------------------------------------


# a container of all the active search requests
var searchRequests : Array = []

# this is the total number of search cycles allocated to the manager. 
# Each update-step these are divided equally amongst all registered path
# requests
var numSearchCyclesPerUpdate : int


func _init(NumCyclesPerUpdate: int):
	numSearchCyclesPerUpdate = NumCyclesPerUpdate

# every time this is called the total amount of search cycles available will
# be shared out equally between all the active path requests. If a search
# requests finishes successfully or fails the method will notify the relevant bot
# This method iterates through all the active path planning requests 
# planning their searches until the user specified total number of search
# cycles has been satisfied.
func UpdateSearches() -> void:
	var NumCyclesRemaining : int = numSearchCyclesPerUpdate
	
	# iterate through the search requests until either all requests have been
	# fulfilled or there are no search cycles remaining for this update-step.
	var index : int = searchRequests.size() - 1
	
	if index < 0:
		return
	
	var curPath = searchRequests[index]
	
	while NumCyclesRemaining > 0 and not searchRequests.empty():
		NumCyclesRemaining -= 1
		
		# make one search cycle of this path request
		var result : int = curPath.CycleOnce()
		
		# if the search has terminated remove from the list
		if result == GraphSearchTimeSliced.TargetFound or result == GraphSearchTimeSliced.TargetNotFound:
			# remove this path from the path list
			searchRequests.erase(curPath)
		
		if searchRequests.empty():
			break
		
		# move on to the next
		index -= 1
		
		# the iterator may now be pointing to the end of the list. If this is so,
		# it must be reset to the beginning.
		if index < 0:
			index = searchRequests.size() - 1
		
		curPath = searchRequests[index]


# a path planner should call this method to register a search with the 
# manager. (The method checks to ensure the path planner is only registered
# once)
func Register(pathPlanner) -> void:
	# make sure the bot does not already have a current search in the queue
	if not pathPlanner in searchRequests:
		# add to the list
		searchRequests.push_front(pathPlanner)


func UnRegister(pathPlanner) -> void:
	searchRequests.erase(pathPlanner)


# returns the amount of path requests currently active.
func GetNumActiveSearches() -> int:
	return searchRequests.size()
