extends Reference
class_name GraphUtils

#----------------------- CreateAllPairsCostsTable -------------------------------
#
#  creates a lookup table of the cost associated from traveling from one
#  node to every other
#-----------------------------------------------------------------------------
static func CreateAllPairsCostsTable(G : SparseGraph) -> Array:
	#create a two dimensional vector
	var row : Array = []
	for i in range(G.NumNodes()):
		row.append(0)
	var PathCosts : Array = []
	for i in range(G.NumNodes()):
		PathCosts.append(row.duplicate())

	for source in range(G.NumNodes()):
		# do the search
		var search : GraphSearchDijkstra = GraphSearchDijkstra.new(G, source)

		# iterate through every node in the graph and grab the cost to travel to
		# that node
		for target in range(G.NumNodes()):
			if source != target:
				PathCosts[source][target]= search.GetCostToNode(target)
		# next target node

	# next source node

	return PathCosts;

#---------------------- CalculateAverageGraphEdgeLength ----------------------
#   determines the average length of the edges in a navgraph (using the 
#   distance between the source & target node positions (not the cost of the 
#   edge as represented in the graph, which may account for all sorts of 
#   other factors such as terrain type, gradients etc)
#------------------------------------------------------------------------------
static func CalculateAverageGraphEdgeLength(G : SparseGraph) -> float:
	var TotalLength : float = 0.0
	var NumEdgesCounted : int = 0
	
	for node in G.nodes:
		if node.index == NavGraphNode.InvalidNodeIndex:
			continue
			
		for edge in G.edges[node.index].keys():
			# increment edge counter
			NumEdgesCounted += 1
			
			# add length of edge to total length
			TotalLength += (G.GetNode(G.edges[node.index][edge].from).position - G.GetNode(G.edges[node.index][edge].to).position).length()

	return TotalLength / NumEdgesCounted

