extends GraphSearchTimeSliced
class_name GraphSearchDijkstraTS
#---------------------------- GraphSearchDijkstraTS --------------------------
#
#   Dijkstra's algorithm class modified to spread a search over multiple
#  update-steps
#-----------------------------------------------------------------------------


var graph : SparseGraph

# indexed into my node. Contains the accumulative cost to that node
var costToThisNode : Array = []

var shortestPathTree : Array = [] # vector of edges
var searchFrontier : Array = [] # vector of edges

var source : int
var target : int

# create an indexed priority queue of nodes. The nodes with the
# lowest overall F cost (G+H) are positioned at the front.
var PQ : IndexedPriorityQLow

var terminationCondition

func _init(Graph : SparseGraph, Source : int, Target : int, TerminationCondition).(SearchType.Dijkstra):  
	graph = Graph
	shortestPathTree.resize(graph.NumNodes())
	searchFrontier.resize(graph.NumNodes())
	costToThisNode.resize(graph.NumNodes())
	source = Source
	target = Target
	
	terminationCondition = TerminationCondition
	
	# create the PQ         ,
	PQ = IndexedPriorityQLow.new(costToThisNode, graph.NumNodes())
	
	# put the source node on the queue
	PQ.insert(source)
	
	for i in range(graph.NumNodes()):
		costToThisNode[i] = 0.0
	

# let the search class take care of tidying up memory (the wary amongst
# you may prefer to use std::auto_ptr or similar to replace the pointer
# to the termination condition)
func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		#terminationCondition.free()
		#PQ.free()
		shortestPathTree.clear()
		searchFrontier.clear()
		costToThisNode.clear()


# When called, this method pops the next node off the PQ and examines all
# its edges. The method returns an enumerated value (target_found,
# target_not_found, search_incomplete) indicating the status of the search
func CycleOnce() -> int:
	# if the PQ is empty the target has not been found
	if PQ.empty():
		return TargetNotFound
	
	# get lowest cost node from the queue
	var NextClosestNode : int = PQ.Pop()
	
	# move this node from the frontier to the spanning tree
	shortestPathTree[NextClosestNode] = searchFrontier[NextClosestNode]
	
	# if the target has been found exit
	if terminationCondition.isSatisfied(graph, target, NextClosestNode):
		# make a note of the node index that has satisfied the condition. This
		# is so we can work backwards from the index to extract the path from
		# the shortest path tree.
		target = NextClosestNode
		
		return TargetFound
	
	# now to test all the edges attached to this node
	var edges : Dictionary = graph.GetEdgesTo(NextClosestNode)
	for edge_key in edges.keys():
		# the total cost to the node this edge points to is the cost to the
		# current node plus the cost of the edge connecting them.
		var NewCost : float= costToThisNode[NextClosestNode] + edges[edge_key].cost
		
		# if this edge has never been on the frontier make a note of the cost
		# to get to the node it points to, then add the edge to the frontier
		# and the destination node to the PQ.
		if searchFrontier[edges[edge_key].to] == null:
			costToThisNode[edges[edge_key].to] = NewCost
			
			PQ.insert(edges[edge_key].to)
			
			searchFrontier[edges[edge_key].to] = edges[edge_key]
		
		# else test to see if the cost to reach the destination node via the
		# current node is cheaper than the cheapest cost found so far. If
		# this path is cheaper, we assign the new cost to the destination
		# node, update its entry in the PQ to reflect the change and add the
		# edge to the frontier
		elif NewCost < costToThisNode[edges[edge_key].to] and shortestPathTree[edges[edge_key].to] == null:
			costToThisNode[edges[edge_key].to] = NewCost
			
			# because the cost is less than it was previously, the PQ must be
			# re-sorted to account for this.
			PQ.ChangePriority(edges[edge_key].to)
			
			searchFrontier[edges[edge_key].to] = edges[edge_key]
	
	# there are still nodes to explore
	return SearchIncomplete

# returns the vector of edges that the algorithm has examined
func GetSPT() -> Array:
	return shortestPathTree

# returns a vector of node indexes that comprise the shortest path
# from the source to the target
func GetPathToTarget() -> Array:
	var path : Array = []
	
	# just return an empty path if no target or no path found
	if target < 0:
		return path
	
	var nd : int = target
	
	path.push_back(nd)
	
	while nd != source and shortestPathTree[nd] != null:
		nd = shortestPathTree[nd].from
		
		path.push_front(nd)
	
	return path
  
# returns the path as a list of PathEdges
func GetPathAsPathEdges() -> Array:
	var path : Array = []
	
	# just return an empty path if no target or no path found
	if target < 0:
		return path;
	
	var nd : int = target
	
	while nd != source and shortestPathTree[nd] != null:
		path.push_front(PathEdge.new(graph.GetNode(shortestPathTree[nd].from).position,
			 graph.GetNode(shortestPathTree[nd].to).position,
			 shortestPathTree[nd].flags,
			 shortestPathTree[nd].idOfIntersectingEntity))
		
		nd = shortestPathTree[nd].from
	
	return path

# returns the total cost to the target
func GetCostToTarget() -> float:
	return costToThisNode[target]
