extends GraphSearchTimeSliced
class_name GraphSearchAStarTS

#-------------------------- Graph_SearchAStar_TS -----------------------------
#  a A* class that enables a search to be completed over multiple update-steps
#-----------------------------------------------------------------------------

var graph : SparseGraph

# indexed into my node. Contains the 'real' accumulative cost to that node
var gCosts : Array = []  # of float

# indexed into by node. Contains the cost from adding gCosts[n] to
# the heuristic cost from n to the target node. This is the vector the
# iPQ indexes into.
var fCosts : Array = []  # of float

var shortestPathTree : Array = []  # of edges
var searchFrontier : Array = []  # of edges

var source : int
var target : int

# create an indexed priority queue of nodes. The nodes with the
# lowest overall F cost (G+H) are positioned at the front.
var PQ : IndexedPriorityQLow

var heuristic

func _init(G : SparseGraph, src : int, tgt : int, Heuristic).(SearchType.A_Star):
	graph = G
	shortestPathTree.resize(G.NumNodes())
	searchFrontier.resize(G.NumNodes())
	gCosts.resize(G.NumNodes())
	fCosts.resize(G.NumNodes())
	source = src
	target = tgt
	
	heuristic = Heuristic
	
	# create the PQ   
	PQ = IndexedPriorityQLow.new(fCosts, graph.NumNodes())

	# put the source node on the queue
	PQ.insert(source)
	
	for i in range(G.NumNodes()):
		gCosts[i] = 0.0
		fCosts[i] = 0.0
	
func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		#heuristic.free()
		#PQ.free()
		shortestPathTree.clear()
		searchFrontier.clear()
		gCosts.clear()
		fCosts.clear()


# When called, this method pops the next node off the PQ and examines all
# its edges. The method returns an enumerated value (target_found,
# target_not_found, search_incomplete) indicating the status of the search
func CycleOnce() -> int:
	# if the PQ is empty the target has not been found
	if PQ.empty():
		return TargetNotFound
	
	# get lowest cost node from the queue
	var NextClosestNode : int = PQ.Pop()
	
	# put the node on the SPT
	shortestPathTree[NextClosestNode] = searchFrontier[NextClosestNode]
	
	# if the target has been found exit
	if NextClosestNode == target:
		return TargetFound
	
	# now to test all the edges attached to this node
	var edges : Dictionary = graph.GetEdgesTo(NextClosestNode)
	for edge_key in edges.keys():
		var edge = edges[edge_key]
		# calculate the heuristic cost from this node to the target (H)                       
		var HCost : float = heuristic.Calculate(graph, target, edge.to) 
		
		# calculate the 'real' cost to this node from the source (G)
		var GCost : float = gCosts[NextClosestNode] + edge.cost
		
		# if the node has not been added to the frontier, add it and update
		# the G and F costs
		if (searchFrontier[edge.to] == null):
			fCosts[edge.to] = GCost + HCost
			gCosts[edge.to] = GCost
			
			PQ.insert(edge.to)
			
			searchFrontier[edge.to] = edge
		
		# if this node is already on the frontier but the cost to get here
		# is cheaper than has been found previously, update the node
		# costs and frontier accordingly.
		elif ((GCost < gCosts[edge.to]) and (shortestPathTree[edge.to] == null)):
			fCosts[edge.to] = GCost + HCost
			gCosts[edge.to] = GCost
			
			PQ.ChangePriority(edge.to)
			
			searchFrontier[edge.to] = edge
	
	# there are still nodes to explore
	return SearchIncomplete

# returns the vector of edges that the algorithm has examined
func GetSPT() -> Array:
	return shortestPathTree

# returns a vector of node indexes that comprise the shortest path
# shortest source to the target
func GetPathToTarget() -> Array:
	var path : Array = []
	
	# just return an empty path if no target or no path found
	if target < 0:
		return path    
	
	var nd : int = target
	
	path.push_back(nd)
	
	while nd != source and shortestPathTree[nd] != null:
		nd = shortestPathTree[nd].from
		path.push_front(nd)
	
	return path

# returns the path as a list of PathEdges
func GetPathAsPathEdges() -> Array:
	var path : Array = []
	
	# just return an empty path if no target or no path found
	if target < 0:
		return path    
	
	var nd : int = target
	
	while nd != source and shortestPathTree[nd] != null:
		path.push_front(PathEdge.new(graph.GetNode(shortestPathTree[nd].from).position,graph.GetNode(shortestPathTree[nd].to).position, shortestPathTree[nd].flags, shortestPathTree[nd].idOfIntersectingEntity))
		nd = shortestPathTree[nd].from

	return path

# returns the total cost to the target
func GetCostToTarget() -> float:
	return gCosts[target]
