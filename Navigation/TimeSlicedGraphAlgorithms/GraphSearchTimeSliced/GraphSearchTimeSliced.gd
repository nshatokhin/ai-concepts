extends Reference
class_name GraphSearchTimeSliced

#------------------------ GraphSearchTimeSliced -----------------------------
#
# base class to define a common interface for graph search algorithms
#-----------------------------------------------------------------------------

enum SearchType {
	A_Star
	Dijkstra
}

# these enums are used as return values from each search update method
enum {
	TargetFound
	TargetNotFound
	SearchIncomplete
}

var searchType : int


func _init(type : int):
	searchType = type
	
#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		pass

# When called, this method runs the algorithm through one search cycle. The
# method returns an enumerated value (target_found, target_not_found,
# search_incomplete) indicating the status of the search
func CycleOnce() -> int:
	return 0

# returns the vector of edges that the algorithm has examined
func GetSPT() -> Array:
	return []

# returns the total cost to the target
func GetCostToTarget() -> float:
	return 0.0

# returns a list of node indexes that comprise the shortest path
# from the source to the target
func GetPathToTarget() -> Array:
	return []

# returns the path as a list of PathEdges
func GetPathAsPathEdges() -> Array:
	return []

