extends Reference
class_name Graph_Node

enum {
	InvalidNodeIndex = -1
}

# every node has an index. A valid index is >= 0
var index : int

func _init(idx : int = InvalidNodeIndex):
	index = idx

func getText() -> String:
	return "Index: " + String(index)
