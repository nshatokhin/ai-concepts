extends Reference
class_name PathEdge

#-----------------------------------------------------------------------------
#
#   Class to represent a path edge. This path can be used by a path
#   planner in the creation of paths. 
#
#-----------------------------------------------------------------------------

# positions of the source and destination nodes this edge connects
var source : Vector2
var destination : Vector2

# the behavior associated with traversing this edge
var behavior : int

var doorID : int

func _init(Source : Vector2, Destination : Vector2, Behavior : int, DoorID : int = 0):
	source = Source
	destination = Destination
	behavior = Behavior
	doorID = DoorID
