extends Reference
class_name SparseGraph

#------------------------------------------------------------------------
#   Graph class using the adjacency list representation.
#------------------------------------------------------------------------

# enable easy client access to the edge and node types used in the graph
# typedef edge_type                EdgeType;
# typedef node_type                NodeType;
  
# a couple more typedefs to save my fingers and to help with the formatting
# of the code on the printed page
# typedef std::vector<node_type>   NodeVector;
# typedef std::list<edge_type>     EdgeList;
# typedef std::vector<EdgeList>    EdgeListVector;

var point_radius = 5

# the nodes that comprise this graph
var nodes : Array = [] # array of NavGraphNode

# a vector of adjacency edge lists. (each node index keys into the 
# list of edges associated with that node)
var edges : Dictionary = {} # dictionary of dictionaries of NavGraphEdge
 
# is this a directed graph?
var digraph : bool

# the index of the next node to be added
var nextNodeIndex : int
  
# returns true if an edge is not already present in the graph. Used
# when adding edges to make sure no duplicates are created.
func UniqueEdge(from : int, to : int) -> bool:
	for curEdge in edges[from].keys():
		if edges[from][curEdge].to == to:
			return false
	
	return true

# iterates through all the edges in the graph and removes any that point
# to an invalidated node
func CullInvalidEdges() -> void:
	for curEdgeList in edges.keys():
		for curEdge in curEdgeList.keys():
			if nodes[curEdge.to].index == NavGraphNode.InvalidNodeIndex or nodes[curEdge.from].index == NavGraphNode.NavGraphNode.InvalidNodeIndex:
				curEdgeList.erase(curEdge)
  
func _init(is_digraph : bool):
	nextNodeIndex = 0
	digraph = is_digraph

# returns the node at the given index
func GetNode(idx : int) -> NavGraphNode:
	if (idx >= nodes.size()) or (idx < 0):
		print("<SparseGraph::GetNode>: invalid index")
		return null

	return nodes[idx]


# if a graph node is removed, it is not removed from the 
# vector of nodes (because that would mean changing all the indices of 
# all the nodes that have a higher index). This method takes a node
# iterator as a parameter and assigns the next valid element to it.
func GetNextValidNodeIndex(var index : int) -> int:
	if (index >= nodes.size()) or (index < 0):
		print("<SparseGraph::GetNextValidNodeIndex>: invalid index")
		return -1
	
	if nodes[index].index != NavGraphNode.InvalidNodeIndex:
		index += 1
		if index >= nodes.size():
			return -1
	
	while nodes[index].index == NavGraphNode.InvalidNodeIndex:
		index += 1
	
		if index >= nodes.size():
			return -1
	
	return index

func GetValidNodeByNum(var index : int) -> NavGraphNode:
	if (index >= NumActiveNodes()) or (index < 0):
		print("<SparseGraph::GetValidNodeByNum>: invalid index")
		return null
		
	var nodeIdx : int = 0
	for node in nodes:
		if node.index != NavGraphNode.InvalidNodeIndex:
			if nodeIdx == index:
				return node
				
			nodeIdx += 1
	
	return null
# method for obtaining a reference to an edge
func GetEdge(from : int, to : int) -> NavGraphEdge:
	if not (from < nodes.size() and from >= 0 and nodes[from].index != NavGraphNode.NavGraphNode.InvalidNodeIndex):
		print("<SparseGraph::GetEdge>: invalid 'from' index")
		return null
	
	if not (to < nodes.size() and to >= 0 and nodes[to].index != NavGraphNode.NavGraphNode.InvalidNodeIndex):
		print("<SparseGraph::GetEdge>: invalid 'to' index")
		return null
	
	for curEdge in edges[from]:
		if curEdge.to == to:
			return curEdge

	print("<SparseGraph::GetEdge>: edge does not exist")
	return null

func GetEdgesFrom(from : int) -> Dictionary:
	return edges[from]
	
func GetEdgesTo(to : int) -> Dictionary:
	return edges[to]

# retrieves the next free node index
func GetNextFreeNodeIndex() -> int:
	return nextNodeIndex
  
# adds a node to the graph and returns its index
# Given a node this method first checks to see if the node has been added
# previously but is now innactive. If it is, it is reactivated.
# If the node has not been added previously, it is checked to make sure its
# index matches the next node index before being added to the graph
func AddNode(node : NavGraphNode) -> int:
	if node.index < nodes.size():
		# make sure the client is not trying to add a node with the same ID as
		# a currently active node
		if not (nodes[node.index].index == NavGraphNode.NavGraphNode.InvalidNodeIndex):
			print("<SparseGraph::AddNode>: Attempting to add a node with a duplicate ID");
			return NavGraphNode.NavGraphNode.InvalidNodeIndex
			
		nodes[node.index] = node
		
		return nextNodeIndex
	else:
		# make sure the new node has been indexed correctly
		if not (node.index == nextNodeIndex):
			print("<SparseGraph::AddNode>:invalid index");
			return NavGraphNode.NavGraphNode.InvalidNodeIndex
		
		nodes.push_back(node)
		edges[node.index] = {}
		
		var idx = nextNodeIndex
		nextNodeIndex += 1
		return idx

#To avoid issues with invalid indices you'll have to go through the array in reverse order:
#
#for i in range(enemies.size() - 1, -1, -1):
#    if (health_to_delete == enemies[i].enemy_health):
#        enemies.remove(i)

# removes a node by setting its index to NavGraphNode.InvalidNodeIndex
# Removes a node from the graph and removes any links to neighbouring
# nodes
func RemoveNode(node : int) -> void:
	if not (node < nodes.size()):
		print("<SparseGraph::RemoveNode>: invalid node index")
		return
	
	# set this node's index to NavGraphNode.InvalidNodeIndex
	nodes[node].index = NavGraphNode.NavGraphNode.InvalidNodeIndex
	
	# if the graph is not directed remove all edges leading to this node and then
	# clear the edges leading from the node
	if not digraph:
		# visit each neighbour and erase any edges leading to this node
		for curEdge in edges[node].keys():
			for curE in edges[curEdge.to].keys():
				if curE.to == node:
					edges[curEdge.to].erase(curE)
					
					break

		# finally, clear this node's edges
		edges[node].clear()
	
	# if a digraph remove the edges the slow way
	else:
		CullInvalidEdges()


# Use this to add an edge to the graph. The method will ensure that the
# edge passed as a parameter is valid before adding it to the graph. If the
# graph is a digraph then a similar edge connecting the nodes in the opposite
# direction will be automatically added.
func AddEdge(edge : NavGraphEdge) -> void:
	# first make sure the from and to nodes exist within the graph 
	if not (edge.from < nextNodeIndex and edge.to < nextNodeIndex):
		print("<SparseGraph::AddEdge>: invalid node index")
		return
	
	# make sure both nodes are active before adding the edge
	if nodes[edge.to].index != NavGraphNode.InvalidNodeIndex and nodes[edge.from].index != NavGraphNode.InvalidNodeIndex:
		# add the edge, first making sure it is unique
		if UniqueEdge(edge.from, edge.to):
			edges[edge.from][edge.to] = edge

		# if the graph is undirected we must add another connection in the opposite
		# direction
		if not digraph:
			# check to make sure the edge is unique before adding
			if UniqueEdge(edge.to, edge.from):
				var NewEdge : NavGraphEdge = NavGraphEdge.new(edge.to, edge.from, edge.cost, edge.flags)
				edges[edge.to][edge.from] = NewEdge

# removes the edge connecting from and to from the graph (if present). If
# a digraph then the edge connecting the nodes in the opposite direction 
# will also be removed.
func RemoveEdge(from : int, to : int) -> void:
	if not (from < nodes.size() and to < nodes.size()):
		print("<SparseGraph::RemoveEdge>:invalid node index")
		return
	
	if not digraph:
		for curEdge in edges[to].keys():
			if curEdge.to == from:
				edges[to].erase(curEdge)
				break

	for curEdge in edges[from].keys():
		if curEdge.to == to:
			edges[from].erase(curEdge)
			break



# sets the cost of an edge
func SetEdgeCost(from : int, to : int, cost : float) -> void:
	# make sure the nodes given are valid
	if not (from < nodes.size() and to < nodes.size()):
		print("<SparseGraph::SetEdgeCost>: invalid index")
		return
	
	# visit each neighbour and erase any edges leading to this node
	for curEdge in edges[from]:
		if curEdge.to == to:
			curEdge.cost = cost
			break


# returns the number of active + inactive nodes present in the graph
func NumNodes() -> int:
	return nodes.size()
  
# returns the number of active nodes present in the graph (this method's
# performance can be improved greatly by caching the value)
func NumActiveNodes() -> int:
	var count : int = 0
	for node in nodes:
		if node.index != NavGraphNode.InvalidNodeIndex:
			count += 1
	return count

# returns the total number of edges present in the graph
func NumEdges() -> int:
	var tot : int = 0
	for curEdge in edges:
		tot += curEdge.size()
	return tot

# returns true if the graph is directed
func isDigraph() -> bool:
	return digraph

# returns true if the graph contains no nodes
func isEmpty() -> bool:
	return nodes.empty()

# returns true if a node with the given index is present in the graph
func isNodePresent(nd : int) -> bool:
	return not (nodes[nd].index == NavGraphNode.InvalidNodeIndex or nd >= nodes.size())

# returns true if an edge connecting the nodes 'to' and 'from'
# is present in the graph
func isEdgePresent(from : int, to : int) -> bool:
	if isNodePresent(from) and isNodePresent(to):
		for curEdge in edges[from]:
			if curEdge.to == to:
				return true
			
		return false
	else:
		return false

# methods for loading and saving graphs from an open file stream or from
# a file name 
#bool  Save(const char* FileName)const;
#bool  Save(std::ofstream& stream)const;
#
#bool  Load(const char* FileName);
#bool  Load(std::ifstream& stream);

# clears the graph ready for new node insertions
func Clear() -> void:
	nextNodeIndex = 0
	nodes.clear()
	edges.clear()

func RemoveEdges() -> void:
	for edge in edges.keys():
		edges[edge].clear()

func draw(canvas : CanvasItem) -> void:
	for node in nodes:
		canvas.draw_circle(node.position, point_radius, Color("#ffff0000"))
	
#    draw_string(font, points[key].position, String(points[key].x) + ":" + String(points[key].y), Color(1, 1, 1))
	
	for fromKey in edges.keys():
		for toKey in edges[fromKey].keys():
			if fromKey == toKey:
				continue

			canvas.draw_line(nodes[edges[fromKey][toKey].from].position, nodes[edges[fromKey][toKey].to].position, Color("#ff00ff00"))
#//-------------------------------- Save ---------------------------------------
#
#template <class node_type, class edge_type>
#bool SparseGraph<node_type, edge_type>::Save(const char* FileName)const
#{
#  //open the file and make sure it's valid
#  std::ofstream out(FileName);
#
#  if (!out)
#  {
#	throw std::runtime_error("Cannot open file: " + std::string(FileName));
#	return false;
#  }
#
#  return Save(out);
#}
#
#//-------------------------------- Save ---------------------------------------
#template <class node_type, class edge_type>
#bool SparseGraph<node_type, edge_type>::Save(std::ofstream& stream)const
#{
#  //save the number of nodes
#  stream << m_Nodes.size() << std::endl;
#
#  //iterate through the graph nodes and save them
#  NodeVector::const_iterator curNode = m_Nodes.begin();
#  for (curNode; curNode!=m_Nodes.end(); ++curNode)
#  {
#	stream << *curNode;
#  }
#
#  //save the number of edges
#  stream << NumEdges() << std::endl;
#
#
#  //iterate through the edges and save them
#  for (int nodeIdx = 0; nodeIdx < m_Nodes.size(); ++nodeIdx)
#  {
#	for (EdgeList::const_iterator curEdge = m_Edges[nodeIdx].begin();
#		 curEdge!=m_Edges[nodeIdx].end(); ++curEdge)
#	{
#	  stream << *curEdge;
#	}  
#  }
#
#  return true;
#}
#
#//------------------------------- Load ----------------------------------------
#//-----------------------------------------------------------------------------
#template <class node_type, class edge_type>
#bool SparseGraph<node_type, edge_type>::Load(const char* FileName)
#{
#  //open file and make sure it's valid
#  std::ifstream in(FileName);
#
#  if (!in)
#  {
#	throw std::runtime_error("Cannot open file: " + std::string(FileName));
#	return false;
#  }
#
#  return Load(in);
#}
#
#//------------------------------- Load ----------------------------------------
#//-----------------------------------------------------------------------------
#template <class node_type, class edge_type>
#bool SparseGraph<node_type, edge_type>::Load(std::ifstream& stream)
#{
#  Clear();
#
#  //get the number of nodes and read them in
#  int NumNodes, NumEdges;
#
#  stream >> NumNodes;
#
#  for (int n=0; n<NumNodes; ++n)
#  {
#	NodeType NewNode(stream);
#
#	//when editing graphs it's possible to end up with a situation where some
#	//of the nodes have been invalidated (their id's set to NavGraphNode.InvalidNodeIndex). Therefore
#	//when a node of index NavGraphNode.InvalidNodeIndex is encountered, it must still be added.
#	if (NewNode.Index() != NavGraphNode.InvalidNodeIndex)
#	{
#	  AddNode(NewNode);
#	}
#	else
#	{
#	  m_Nodes.push_back(NewNode);
#
#	  //make sure an edgelist is added for each node
#	  m_Edges.push_back(EdgeList());
#
#	  ++m_iNextNodeIndex;
#	}
#  }
#
#  //now add the edges
#  stream >> NumEdges;
#  for (int e=0; e<NumEdges; ++e)
#  {
#	EdgeType NextEdge(stream);
#
#	m_Edges[NextEdge.From()].push_back(NextEdge);
#  }
#
#  return true;
#}
