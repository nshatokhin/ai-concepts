extends Graph_Node
class_name NavGraphNode

#-----------------------------------------------------------------------------
#  Graph node for use in creating a navigation graph.This node contains
#  the position of the node and a pointer to a BaseGameEntity... useful
#  if you want your nodes to represent health packs, gold mines and the like
#-----------------------------------------------------------------------------

# the node's position
var position : Vector2

# often you will require a navgraph node to contain additional information.
# For example a node might represent a pickup such as armor in which
# case m_ExtraInfo could be an enumerated value denoting the pickup type,
# thereby enabling a search algorithm to search a graph for specific items.
# Going one step further, m_ExtraInfo could be a pointer to the instance of
# the item type the node is twinned with. This would allow a search algorithm
# to test the status of the pickup during the search. 
var extraInfo

func _init(idx : int = Graph_Node.InvalidNodeIndex, pos : Vector2 = Vector2(), ExtraInfo = null).(idx):
	position = pos
	extraInfo = ExtraInfo


func getText() -> String:
	return "Index: " + String(index) + " PosX: " + String(position.x) + " PosY: " + String(position.y)

