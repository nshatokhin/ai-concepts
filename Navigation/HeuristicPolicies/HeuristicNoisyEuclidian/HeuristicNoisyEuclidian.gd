extends Reference
class_name HeuristicNoisyEuclidian

#-----------------------------------------------------------------------------
# this uses the euclidian distance but adds in an amount of noise to the 
# result. You can use this heuristic to provide imperfect paths. This can
# be handy if you find that you frequently have lots of agents all following
# each other in single file to get from one place to another
#-----------------------------------------------------------------------------

var rng : RandomNumberGenerator = RandomNumberGenerator.new()

func _init():
	rng.randomize()

# calculate the straight line distance from node nd1 to node nd2
func Calculate(G : SparseGraph, nd1 : int, nd2 : int) -> float:
	return (G.GetNode(nd1).position - G.GetNode(nd2).position).length() * rng.randf_range(0.9, 1.1)
