extends Reference
class_name HeuristicDijkstra

#-----------------------------------------------------------------------------
# you can use this class to turn the A* algorithm into Dijkstra's search.
# this is because Dijkstra's is equivalent to an A* search using a heuristic
# value that is always equal to zero.
#-----------------------------------------------------------------------------
func Calculate(G : SparseGraph, nd1 : int, nd2 : int) -> float:
	return 0.0
	
