extends Reference
class_name HeuristicEuclid

#-----------------------------------------------------------------------------
#    the euclidian heuristic (straight-line distance)
#-----------------------------------------------------------------------------

# calculate the straight line distance from node nd1 to node nd2

func Calculate(G : SparseGraph, nd1 : int, nd2 : int) -> float:
	return (G.GetNode(nd1).position - G.GetNode(nd2).position).length()
