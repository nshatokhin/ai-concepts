extends GraphEdge
class_name NavGraphEdge

#examples of typical flags
enum {
	Normal            = 0,
	Swim              = 1 << 0,
	Crawl             = 1 << 1,
	Creep             = 1 << 3,
	Jump              = 1 << 3,
	Fly               = 1 << 4,
	Grapple           = 1 << 5,
	GoesThroughDoor = 1 << 6
}

var flags : int

# if this edge intersects with an object (such as a door or lift), then
# this is that object's ID. 
var idOfIntersectingEntity : int

func _init(From : int, To : int, Cost : float, Flags : int = 0, id : int = -1).(From, To, Cost):
	flags = Flags
	idOfIntersectingEntity = id
