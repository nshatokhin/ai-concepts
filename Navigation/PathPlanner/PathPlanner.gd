extends Reference
class_name PathPlanner

#-----------------------------------------------------------------------------
#     Class to handle the creation of paths through a navigation graph
#-----------------------------------------------------------------------------

# for legibility
enum {
	NoClosestNodeFound = -1
}

#  //for ease of use typdef the graph edge/node types used by the navgraph
#  typedef Raven_Map::NavGraph::EdgeType           EdgeType;
#  typedef Raven_Map::NavGraph::NodeType           NodeType;
#  typedef std::list<PathEdge>                     Path;
  
signal NoPathAvailable
signal PathReady(trigger)

# A pointer to the owner of this class
var agent

# a reference to the navgraph
var navGraph : SparseGraph

# a pointer to an instance of the current graph search algorithm.
var currentSearch : GraphSearchTimeSliced

# this is the position the bot wishes to plan a path to reach
var destinationPos : Vector2


# returns the index of the closest visible and unobstructed graph node to
# the given position
func GetClosestNodeToPosition(pos : Vector2) -> int:
	var ClosestSoFar : float = Constants.MaxFloat
	var ClosestNode : int = NoClosestNodeFound
	
	# when the cell space is queried this the the range searched for neighboring
	# graph nodes. This value is inversely proportional to the density of a 
	# navigation graph (less dense = bigger values)
	#var rng : float = WorldController.cellSpaceNeighborhoodRange
	
	# calculate the graph nodes that are neighboring this position
	#WorldController.GetCellSpace().CalculateNeighbors(pos, rng)
	
	# iterate through the neighbors and sum up all the position vectors
	for node in WorldController.graph.nodes:#WorldController.GetCellSpace():
		# if the path between this node and pos is unobstructed calculate the
		# distance
		if node.index == NavGraphNode.InvalidNodeIndex:
			continue
		
		if agent.canWalkBetween(pos, node.position):
			var dist : float = (pos - node.position).length_squared()
			
			# keep a record of the closest so far
			if dist < ClosestSoFar:
				ClosestSoFar = dist;
				ClosestNode  = node.index
	
	return ClosestNode


# smooths a path by removing extraneous edges. (may not remove all
# extraneous edges)
func SmoothPathEdgesQuick(path : Array) -> Array: # returns array of PathEdge
	# create a couple of iterators and point them at the front of the path
	var e2 : int = path.size()-1
	var e1 : int = e2
	
	# increment e2 so it points to the edge following e1.
	e1 -= 1
	
	# while e2 is not the last edge in the path, step through the edges checking
	# to see if the agent can move without obstruction from the source node of
	# e1 to the destination node of e2. If the agent can move between those 
	# positions then the two edges are replaced with a single edge.
	while e1 >= 0:
		# check for obstruction, adjust and remove the edges accordingly
		if path[e2].behavior == NavGraphEdge.Normal and agent.canWalkBetween(path[e1].source, path[e2].destination):
			path[e1].destination = path[e2].destination
			path.remove(e2)
			e2 -= 1
			e1 -= 1
		else:
			e2 = e1
			e1 -= 1
			
	return path

# smooths a path by removing extraneous edges. (removes *all* extraneous
# edges)
func SmoothPathEdgesPrecise(path : Array) -> Array: # returns array of PathEdge
	  # create a couple of iterators
	var e1 : int = path.size()-1
	var e2 : int = e1
	
	while e2 >= 0:
		# point e2 to the edge immediately following e1
		e1 = e2; 
		e1 -= 1
		
		# while e2 is not the last edge in the path, step through the edges
		# checking to see if the agent can move without obstruction from the 
		# source node of e1 to the destination node of e2. If the agent can move
		# between those positions then the any edges between e1 and e2 are
		# replaced with a single edge.
		while e1 >= 0:
			# check for obstruction, adjust and remove the edges accordingly
			if path[e2].behavior == NavGraphEdge.Normal and agent.canWalkBetween(path[e1].source, path[e2].destination):
				path[e1].destination = path[e2].destination
				path.remove(e2)
				e2 = e1
				e1 -= 1
			else:
				e1 -= 1
			
		e2 -= 1
	
	return path


# called at the commencement of a new search request. It clears up the 
# appropriate lists and memory in preparation for a new search request
func GetReadyForNewSearch() -> void:
	if not currentSearch:
		return
		
	# unregister any existing search with the path manager
	WorldController.pathManager.UnRegister(self)
	
	# clean up memory used by any existing search
	#currentSearch.free()
	currentSearch = null


# Given an item type, this method determines the closest reachable graph node
# to the bot's position and then creates a instance of the time-sliced 
# Dijkstra's algorithm, which it registers with the search manager
func RequestPathToItem(ItemType : int) -> bool:
	# clear the waypoint list and delete any active search
	GetReadyForNewSearch()
	return false
	# find the closest visible node to the bots position
	var ClosestNodeToBot : int = GetClosestNodeToPosition(agent.global_position)
	
	# remove the destination node from the list and return false if no visible
	# node found. This will occur if the navgraph is badly designed or if the bot
	# has managed to get itself *inside* the geometry (surrounded by walls),
	# or an obstacle
	if ClosestNodeToBot == NoClosestNodeFound:
		if UserOptions.ShowNavInfo:
			print("No closest node to bot found!")
		
		return false
	
	# create an instance of the search algorithm
	currentSearch = GraphSearchDijkstraTS.new(navGraph, ClosestNodeToBot, ItemType, FindActiveTriggerTerminationPolicy.new())  
	
	# register the search with the path manager
	WorldController.pathManager.Register(self)
	
	return true

# Given a target, this method first determines if nodes can be reached from 
# the  bot's current position and the target position. If either end point
# is unreachable the method returns false. 
# If nodes are reachable from both positions then an instance of the time-
# sliced A* search is created and registered with the search manager. the
# method then returns true.
func RequestPathToPosition(TargetPos : Vector2) -> bool:
#	if UserOptions.ShowNavInfo:
#		print("RequestPathToPosition: temporary disabled, always return true")

	GetReadyForNewSearch()
	
	# make a note of the target position.
	destinationPos = TargetPos
	
	# if the target is walkable from the bot's position a path does not need to
	# be calculated, the bot can go straight to the position by ARRIVING at
	# the current waypoint
	if agent.canWalkTo(TargetPos):
		return true
	
	# find the closest visible node to the bots position
	var ClosestNodeToBot : int = GetClosestNodeToPosition(agent.global_position)
	
	# remove the destination node from the list and return false if no visible
	# node found. This will occur if the navgraph is badly designed or if the bot
	# has managed to get itself *inside* the geometry (surrounded by walls),
	# or an obstacle.
	if ClosestNodeToBot == NoClosestNodeFound:
		if UserOptions.ShowNavInfo:
			print("No closest node to bot found!")
		
		return false
	
	if UserOptions.ShowNavInfo:
		print("Closest node to bot is ", ClosestNodeToBot)
	
	# find the closest visible node to the target position
	var ClosestNodeToTarget : int = GetClosestNodeToPosition(TargetPos)
	
	# return false if there is a problem locating a visible node from the target.
	# This sort of thing occurs much more frequently than the above. For
	# example, if the user clicks inside an area bounded by walls or inside an
	# object.
	if ClosestNodeToTarget == NoClosestNodeFound:
		if UserOptions.ShowNavInfo:
			print("No closest node to target (", ClosestNodeToTarget, ") found!")
		
		return false
	
	if UserOptions.ShowNavInfo:
		print("Closest node to target is ", ClosestNodeToTarget)
	
	# create an instance of a the distributed A* search class
	currentSearch = GraphSearchAStarTS.new(navGraph, ClosestNodeToBot, ClosestNodeToTarget, HeuristicEuclid.new())
	
	# and register the search with the path manager
	WorldController.pathManager.Register(self)
	
	return true

# called by an agent after it has been notified that a search has terminated
# successfully. The method extracts the path from currentSearch, adds
# additional edges appropriate to the search type and returns it as a list of
# PathEdges.
func GetPath() -> Array:
	if not currentSearch:
		print("<PathPlanner::GetPathAsNodes>: no current search");
		return []
	
	var path : Array =  currentSearch.GetPathAsPathEdges()
	
	var closest : int = GetClosestNodeToPosition(agent.global_position)
	
	if closest == NavGraphNode.InvalidNodeIndex:
		print("<PathPlanner::GetPathAsNodes>: no closest node found");
		return []
	
	path.push_front(PathEdge.new(agent.global_position, GetNodePosition(closest), NavGraphEdge.Normal))
	
	
	# if the bot requested a path to a location then an edge leading to the
	# destination must be added
	if currentSearch.searchType == GraphSearchTimeSliced.SearchType.A_Star:
		path.push_back(PathEdge.new(path.back().destination, destinationPos, NavGraphEdge.Normal))
	
	# smooth paths if required
	if UserOptions.SmoothPathsQuick:
		SmoothPathEdgesQuick(path)
	
	if UserOptions.SmoothPathsPrecise:
		SmoothPathEdgesPrecise(path)
	
	return path

# returns the cost to travel from the bot's current position to a specific 
# graph node. This method makes use of the pre-calculated lookup table
# created by Raven_Game
func GetCostToNode(NodeIdx : int) -> float:
	# find the closest visible node to the bots position
	var nd : int = GetClosestNodeToPosition(agent.global_position)
	
	# add the cost to this node
	var cost : float = (agent.global_position - navGraph.GetNode(nd).position).length()
	
	# add the cost to the target node and return
	return cost + navGraph.CalculateCostToTravelBetweenNodes(nd, NodeIdx)


# returns the cost to the closest instance of the GiverType. This method
# also makes use of the pre-calculated lookup table. Returns -1 if no active
# trigger found
func GetCostToClosestItem(GiverType : int) -> float:
	# find the closest visible node to the bots position
	var nd : int = GetClosestNodeToPosition(agent.global_position)
	
	# if no closest node found return failure
	if nd == NavGraphNode.InvalidNodeIndex:
		return -1.0
	
	var ClosestSoFar : float = Constants.MaxFloat
	
	# iterate through all the triggers to find the closest *active* trigger of 
	# type GiverType
	var triggers : Array = WorldController.triggers
	
	for trigger in triggers:
		if trigger.triggerType == Trigger.Type.Giver and trigger.isActive():
			var cost : float = WorldController.CalculateCostToTravelBetweenNodes(nd, trigger.GraphNodeIndex())
			
			if cost < ClosestSoFar:
				ClosestSoFar = cost
				
	# return a negative value if no active trigger of the type found
	if Utils.isEqual(ClosestSoFar, Constants.MaxFloat):
		return -1.0
	
	return ClosestSoFar


# the path manager calls this to iterate once though the search cycle
# of the currently assigned search algorithm. When a search is terminated
# the method messages the owner with either the msg_NoPathAvailable or
# msg_PathReady messages
func CycleOnce() -> int:
	if not currentSearch:
		print("<PathPlanner::CycleOnce>: No search object instantiated")
		return GraphSearchTimeSliced.TargetNotFound
	
	var result : int = currentSearch.CycleOnce()
	
	# let the bot know of the failure to find a path
	if result == GraphSearchTimeSliced.TargetNotFound:
		emit_signal("NoPathAvailable")
	
	# let the bot know a path has been found
	elif result == GraphSearchTimeSliced.TargetFound:
		# if the search was for an item type then the final node in the path will
		# represent a giver trigger. Consequently, it's worth passing the pointer
		# to the trigger in the extra info field of the message. (The pointer
		# will just be NULL if no trigger)
		var trigger = navGraph.GetNode(currentSearch.GetPathToTarget().back()).extraInfo
		
		emit_signal("PathReady", trigger)

	return result



# used to retrieve the position of a graph node from its index. (takes
# into account the enumerations 'non_graph_source_node' and 
# 'non_graph_target_node'
func GetNodePosition(idx : int) -> Vector2:
	return navGraph.GetNode(idx).position


#---------------------------- ctor -------------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, graph : SparseGraph):
	agent = bot_agent
	navGraph = graph
	currentSearch = null


#-------------------------- dtor ---------------------------------------------
#-----------------------------------------------------------------------------
#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		GetReadyForNewSearch()
