extends Node

export (int) var MinInt : int = -2147483648
export (int) var MaxInt : int = 2147483647
export (float) var MinFloat : float = 1.17549435e-38
export (float) var MaxFloat : float = 1.70141183e+38

export (float) var Epsilon : float = 1e-12

export (float) var Pi : float = PI
export (float) var HalfPi : float = PI / 2.0
