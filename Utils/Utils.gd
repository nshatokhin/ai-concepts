extends Node

#-----------------------------------------------------------------------
#  
#  some handy little functions
#-----------------------------------------------------------------------


func Sigmoid(input : float, response : float = 1.0) -> float:
	return ( 1.0 / ( 1.0 + exp(-input / response)))

# compares two real numbers. Returns true if they are equal
func isEqual(a : float, b : float) -> bool:
	return (abs(a-b) < Constants.Epsilon)
