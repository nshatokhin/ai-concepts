extends Node

# this is the maximum number of search cycles allocated to *all* current path
# planning searches per update
var MaxSearchCyclesPerUpdateStep : int = 1000

var DebugDraw : bool = true
var ShowNavInfo : bool = true

var SmoothPathsQuick : bool = false
var SmoothPathsPrecise : bool = false

var TriggerTestRegulatorTime : float = 1.0
var TriggerExplosionTime : float = 0.3
var DefaultGiverTriggerRange : float = 30.0
var HealthRespawnDelay : float = 10.0
