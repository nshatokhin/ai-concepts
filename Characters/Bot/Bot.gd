extends KinematicBody2D
class_name Bot

enum BotStatus {
	Alive = 0
	Dead = 1
	Spawning = 2
}

export (Vector2) var velocity : = Vector2()
  
# a normalized vector pointing in the direction the entity is heading. 
export (Vector2) var heading : = Vector2(1, 0) setget SetHeading

# a vector perpendicular to the heading vector
onready var side : = Vector2(-heading.y, heading.x)

export (float) var mass = 1.0

export (float) var MaxSpeed = 100.0
# special movement speeds (unused)
export (float) var MaxSwimmingSpeed = MaxSpeed * 0.2
export (float) var MaxCrawlingSpeed = MaxSpeed * 0.6
  
# the maximum speed this entity may travel at.
export (float) var maxSpeed = MaxSpeed

# the maximum force this entity can produce to power itself 
# (think rockets and thrust)
export (float) var maxForce = 50.0
  
# the maximum rate (radians per second)this vehicle can rotate         
export (float) var maxTurnRate = 2.0

# the bot's field of view (in degrees)
export (float) var FOV = 180.0

# how long (in seconds) a bot's sensory memory persists
export (float) var MemorySpan  = 5.0
# the bot's reaction time (in seconds)
export (float) var ReactionTime = 0.2
# how accurate the bots are at aiming. 0 is very accurate, (the value represents
# the max deviation in range (in radians))
export (float) var AimAccuracy = 0.0
# how long (in seconds) the bot will keep pointing its weapon at its target
# after the target goes out of view
export (float) var AimPersistance = 1.0

# the number of times a second a bot 'thinks' about weapon selection
export (float) var WeaponSelectionFrequency = 2.0
# the number of times a second a bot 'thinks' about changing strategy
export (float) var GoalAppraisalUpdateFreq = 4.0
# the number of times a second a bot updates its target info
export (float) var TargetingUpdateFreq = 2.0
# the number of times a second the triggers are updated
export (float) var TriggerUpdateFreq = 8.0
# the number of times a second a bot updates its vision
export (float) var VisionUpdateFreq = 4.0
# note that a frequency of -1 will disable the feature and a frequency of zero
# will ensure the feature is updated every bot update

# how long a flash is displayed when the bot is hit
export (float) var HitFlashTime = 0.2

const steeringParams : Resource = preload("res://Steering/DefaultParams.tres")
var steering : SteeringBehavior

onready var brain : GoalComposite = GoalThink.new(self)

var status : int = BotStatus.Dead

# the bot's health. Every time the bot is shot this value is decreased. If
# it reaches zero then the bot dies (and respawns)
var health : int

# the bot's maximum health value. It starts its life with health at this value
var maxHealth : int = 100

# each time this bot kills another this value is incremented
var score : int

# the direction the bot is facing (and therefore the direction of aim). 
# Note that this may not be the same as the bot's heading, which always
# points in the direction of the bot's movement
var facing : Vector2 = heading

# a vector perpendicular to the facing vector
onready var facingSide : = facing.rotated(deg2rad(90.0))

# a bot only perceives other bots within this field of view
var fieldOfView : float

# to show that a player has been hit it is surrounded by a thick 
# red circle for a fraction of a second. This variable represents
# the number of update-steps the circle gets drawn
var numUpdatesHitPersistant : int

# set to true when the bot is hit, and remains true until 
# numUpdatesHitPersistant becomes zero. (used by the render method to
# draw a thick red circle around a bot to indicate it's been hit)
var hit : bool = false

# set to true when a human player takes over control of the bot
var possessed : bool = false

var targetSystem : TargetingSystem
var sensoryMem : SensoryMemory
var weaponSys : WeaponSystem

# A regulator object limits the update frequency of a specific AI component
var weaponSelectionRegulator : Regulator
var goalArbitrationRegulator : Regulator
var targetSelectionRegulator : Regulator
var triggerTestRegulator : Regulator
var visionUpdateRegulator : Regulator

# the bot uses this to plan paths
var pathPlanner : PathPlanner

onready var sprite = $AnimatedSprite
onready var collisionShape = $CollisionShape2D
onready var goalsRotation = $GoalsRotation
onready var goalsLabel = $GoalsRotation/GoalsPosition/Label
onready var evaluatorsLabel = $GoalsRotation/GoalsPosition/EvaluatorsLabel
onready var weaponNode = $WeaponNode
onready var healthBar = $GoalsRotation/GoalsPosition/HealthBar
onready var pathfindingIcon = $PathFindindIcon

export (PoolVector2Array) var patrolPath : PoolVector2Array = PoolVector2Array()
var _neighbors : Array = []

func neighbors() -> Array:
	return _neighbors

func _ready():
	fieldOfView = FOV
	
	steering = SteeringBehavior.new(self, steeringParams)
	
	SetAlive()
	
	# create the navigation module
	pathPlanner = PathPlanner.new(self, WorldController.graph)
	
	# create the regulators
	weaponSelectionRegulator = Regulator.new(WeaponSelectionFrequency)
	goalArbitrationRegulator =  Regulator.new(GoalAppraisalUpdateFreq)
	targetSelectionRegulator = Regulator.new(TargetingUpdateFreq)
	triggerTestRegulator = Regulator.new(TriggerUpdateFreq)
	visionUpdateRegulator = Regulator.new(VisionUpdateFreq)
	
	brain.RemoveAllSubgoals()
	
	# create the targeting system
	targetSystem = TargetingSystem.new(self)
	targetSystem.ClearTarget()
	
	sensoryMem = SensoryMemory.new(self, MemorySpan)

	weaponSys = WeaponSystem.new(self, ReactionTime, AimAccuracy, AimPersistance)
	weaponSys.Initialize()
	RestoreHealthToMaximum()
	
func _physics_process(delta):
	# process the currently active goal. Note this is required even if the bot
	# is under user control. This is because a goal is created whenever a user 
	# clicks on an area of the map that necessitates a path planning request.
	brain.Process()
	
	# Calculate the steering force and update the bot's velocity and position
	UpdateMovement()
	
	# if the bot is under AI control but not scripted
	if not isPossessed():
		# examine all the opponents in the bots sensory memory and select one
		# to be the current target
		if targetSelectionRegulator.isReady():
			targetSystem.Update()
		
		# appraise and arbitrate between all possible high level goals
		if goalArbitrationRegulator.isReady():
			brain.Arbitrate()
		
		# update the sensory memory with any visual stimulus
		if visionUpdateRegulator.isReady():
			sensoryMem.UpdateVision()
			
		# select the appropriate weapon to use from the weapons currently in
		# the inventory
		if weaponSelectionRegulator.isReady():
			weaponSys.SelectWeapon()
		
		# this method aims the bot's current weapon at the current target
		# and takes a shot if a shot is possible
		weaponSys.TakeAimAndShoot()

	rotation = heading.angle()
	goalsRotation.rotation = -rotation
	
	goalsLabel.text = brain.getGoalsText()
	evaluatorsLabel.text = brain.GetEvaluations()
	healthBar.value = health
	
	weaponNode.rotation = facing.rotated(-rotation).angle()

	if hit:
		sprite.modulate = Color(1, 0, 0)
		
		if numUpdatesHitPersistant <= 0:
			hit = false
	else:
		if possessed:
			sprite.modulate = Color(0, 1, 0)
		else:
			sprite.modulate = Color(1, 1, 1)
	
	numUpdatesHitPersistant -= 1
	
	update()

func _draw():
	if UserOptions.DebugDraw:
		brain.draw(self as CanvasItem)
		steering.draw(self as CanvasItem)

func boundingRadius() -> float:
	return max(collisionShape.shape.radius, collisionShape.shape.height/2)

func RestoreHealthToMaximum() -> void:
	health = maxHealth
	healthBar.max_value = maxHealth
	healthBar.value = health

func IncreaseHealth(value : int):
	health += value 
	health = clamp(health, 0, maxHealth);
	healthBar.max_value = maxHealth
	healthBar.value = health

func ReduceHealth(val : int) -> void:
	health -= val
	
	if health <= 0:
		SetDead()
	
	hit = true
	
#	numUpdatesHitPersistant = (int)(FrameRate * script->GetDouble("HitFlashTime"));

func AddWeapon(weapon : int):
	weaponSys.AddWeapon(weapon)
	
func SetWeapon(weapon : int):
	weaponSys.ChangeWeapon(weapon)

func IsSpeedMaxedOut() -> bool:
	return maxSpeed*maxSpeed >= velocity.length_squared()
	
func Speed() -> float:
	return velocity.length()
	
func SpeedSq() -> float:
	return velocity.length_squared()
  
func Heading() -> Vector2:
	return heading

func IncrementScore() -> void: 
	++score

func isPossessed() -> bool:
	return possessed

func isDead() -> bool:
	return status == BotStatus.Dead
	
func isAlive() -> bool:
	return status == BotStatus.Alive
	
func isSpawning() -> bool:
	return status == BotStatus.Spawning
  
func SetSpawning() -> void:
	status = BotStatus.Spawning
	
func SetDead() -> void:
	status = BotStatus.Dead
	
func SetAlive() -> void:
	status = BotStatus.Alive

#------------------------- UpdateMovement ------------------------------------
#
#  this method is called from the update method. It calculates and applies
#  the steering force for this time-step.
#-----------------------------------------------------------------------------
func UpdateMovement() -> void:
	# calculate the combined steering force
	var force : Vector2 = steering.Calculate()

	# if no steering force is produced decelerate the player by applying a
	# braking force
	if steering.steeringForce.length() == 0:
		var BrakingRate : float = 0.8
		
		velocity = velocity * BrakingRate
	
	# calculate the acceleration
	var accel : Vector2 = force / mass
	
	# update the velocity
	velocity += accel
	
	# make sure vehicle does not exceed maximum velocity
	velocity = velocity.clamped(maxSpeed)
	
	# if the vehicle has a non zero velocity the heading and side vectors must 
	# be updated
	if velocity.length() > 0:
		heading = velocity.normalized()
		
		side = heading.rotated(deg2rad(90.0))
	
	# update the position
	#global_position += velocity;
	velocity = move_and_slide(velocity)

#--------------------------- RotateHeadingToFacePosition ---------------------
#
#  given a target position, this method rotates the entity's heading and
#  side vectors by an amount not greater than m_dMaxTurnRate until it
#  directly faces the target.
#
#  returns true when the heading is facing in the desired direction
#-----------------------------------------------------------------------------
func RotateHeadingToFacePosition(target : Vector2) -> bool:
	var toTarget : Vector2 = (target - global_position).normalized()

	# first determine the angle between the heading vector and the target
	var angle : float = toTarget.angle() - heading.angle()
	
	# return true if the player is facing the target
	if angle < 0.00001:
		return true
	
	# clamp the amount to turn to the max turn rate
	if angle > maxTurnRate:
		angle = maxTurnRate
		
	heading = heading.rotated(angle)
	velocity = velocity.rotated(angle)
	
	# finally recreate side
	side = heading.rotated(deg2rad(90.0))
	
	return false


#------------------------- SetHeading ----------------------------------------
#
#  first checks that the given heading is not a vector of zero length. If the
#  new heading is valid this fumction sets the entity's heading and side 
#  vectors accordingly
#-----------------------------------------------------------------------------
func SetHeading(new_heading : Vector2) -> void:
  heading = new_heading.normalized()

  # the side vector must always be perpendicular to the heading
  side = heading.rotated(deg2rad(90.0))

#------------------ RotateFacingTowardPosition -------------------------------
# 
#   given a target position, this method rotates the bot's facing vector
#   by an amount not greater than m_dMaxTurnRate until it
#   directly faces the target.
# 
#   returns true when the heading is facing in the desired direction
#----------------------------------------------------------------------------
func RotateFacingTowardPosition(target : Vector2) -> bool:
	var toTarget : Vector2 = (target - global_position).normalized()

	# determine the angle between the facing vector and the target
	var angle : float = toTarget.angle() - facing.angle()
	
	# return true if the bot's facing is within WeaponAimTolerance degs of
	# facing the target
	var WeaponAimTolerance : float = 0.01; # 2 degs approx
	
	if angle < WeaponAimTolerance:
		facing = toTarget
		facingSide = facing.rotated(deg2rad(90.0))
		return true
	
	# clamp the amount to turn to the max turn rate
	if angle > maxTurnRate:
		angle = maxTurnRate
		
	facing = facing.rotated(angle)
	facingSide = facing.rotated(deg2rad(90.0))
	
	return false

#--------------------------- Possess -----------------------------------------
#
#  this is called to allow a human player to control the bot
#-----------------------------------------------------------------------------
func TakePossession() -> void:
	if not (isSpawning() or isDead()):
		possessed = true;
		
		print("Player Possesses bot ", self)

#------------------------------- Exorcise ------------------------------------
# 
#  called when a human is exorcised from this bot and the AI takes control
#-----------------------------------------------------------------------------
func Exorcise() -> void:
	possessed = false
	
	# when the player is exorcised then the bot should resume normal service
	brain.AddGoal_Explore()
	
	print("Player is exorcised from bot ", self)


#----------------------- ChangeWeapon ----------------------------------------
func ChangeWeapon(type : int) -> void:
	weaponSys.ChangeWeapon(type)


#---------------------------- FireWeapon -------------------------------------
#
#  fires the current weapon at the given position
#-----------------------------------------------------------------------------
func FireWeapon(pos : Vector2) -> void:
	weaponSys.ShootAt(pos)


#----------------- CalculateExpectedTimeToReachPosition ----------------------
#
#  returns a value indicating the time in seconds it will take the bot
#  to reach the given position at its current speed.
#-----------------------------------------------------------------------------
func CalculateTimeToReachPosition(pos : Vector2, dt : float = 1.0) -> float:
	return (global_position - pos).length() / (maxSpeed)# * Engine.get_frames_per_second()) #FrameRate


#------------------------ isAtPosition ---------------------------------------
#
#  returns true if the bot is close to the given position
#-----------------------------------------------------------------------------
func isAtPosition(pos : Vector2) -> bool:
	var tolerance : float = 10.0

	return (global_position - pos).length_squared() < tolerance * tolerance

#------------------------- hasLOSt0 ------------------------------------------
#
#  returns true if the bot has line of sight to the given position.
#-----------------------------------------------------------------------------
func hasLOSto(pos : Vector2, bot_agent = null) -> bool:
	var space_state : = get_world_2d().direct_space_state
	var exclude = [self]
	if bot_agent:
		exclude.push_back(bot_agent)
	var result = space_state.intersect_ray(global_position, pos, exclude)
	return result.empty() #m_pWorld->isLOSOkay(global_position, pos);

func hasLOStoBot(bot_agent) -> bool:
	var space_state : = get_world_2d().direct_space_state
	var intersection_info : = space_state.intersect_ray(global_position, bot_agent.global_position, [self, bot_agent])

	return intersection_info.empty()
# returns true if this bot can move directly to the given position
# without bumping into any walls
func canWalkTo(pos : Vector2) -> bool:
	return canWalkBetween(global_position, pos)

# similar to above. Returns true if the bot can move between the two
# given positions without bumping into any walls
func canWalkBetween(from : Vector2, to : Vector2) -> bool:
	# return true #!m_pWorld->isPathObstructed(from, to, boundingRadius());
	
#	var space_state = get_world_2d().direct_space_state
#	var motion = to - from
#	var query = Physics2DShapeQueryParameters.new()
#	query.motion = motion
#	query.set_shape(collisionShape.shape) 
##	query.collision_layer = get_parent().collision_layer
#	query.exclude = [self]
##	query.transform = transform
#	var result = space_state.cast_motion(query)
#	
#	return (not result.empty()) and result[0] == 1.0 and result[1] == 1.0
 #!m_pWorld->isPathObstructed(global_position, pos, boundingRadius());
	var t = transform
	t.origin = from
	var res = not test_move(t, to - from)#(Transform2D((to - from).angle(), from), to, false)
	return res


#--------------------------- canStep Methods ---------------------------------
#
#  returns true if there is space enough to step in the indicated direction
#  If true PositionOfStep will be assigned the offset position
#-----------------------------------------------------------------------------
func canStepLeft(PositionOfStep : Vector2) -> bool:
	var StepDistance : float = boundingRadius() * 2
	var facingPerp : Vector2 = facing.rotated(deg2rad(90.0))
	PositionOfStep = global_position - facingPerp * StepDistance - facingPerp * boundingRadius()
	
	return canWalkTo(PositionOfStep)

func canStepRight(PositionOfStep : Vector2) -> bool:
	var StepDistance : float = boundingRadius() * 2
	var facingPerp : Vector2 = facing.rotated(deg2rad(90.0))
	PositionOfStep = global_position + facingPerp * StepDistance + facingPerp * boundingRadius()
	
	return canWalkTo(PositionOfStep)

func canStepForward(PositionOfStep : Vector2) -> bool:
	var StepDistance : float = boundingRadius() * 2
	
	PositionOfStep = global_position + facing * StepDistance + facing * boundingRadius()
	
	return canWalkTo(PositionOfStep)

func canStepBackward(PositionOfStep : Vector2) -> bool:
	var StepDistance : float = boundingRadius() * 2
	
	PositionOfStep = global_position - facing * StepDistance - facing * boundingRadius()
	
	return canWalkTo(PositionOfStep)


func _on_PerceptionArea_body_entered(body):
	if body.is_in_group("Bots") and (not body in _neighbors) and body != self:
		_neighbors.push_back(body)


func _on_PerceptionArea_body_exited(body):
	if body in _neighbors:
		_neighbors.erase(body)


#------------------------------- Spawn ---------------------------------------
#
#   spawns the bot at the given position
#-----------------------------------------------------------------------------
func Spawn(pos : Vector2) -> void:
	SetAlive()
	brain.RemoveAllSubgoals()
	targetSystem.ClearTarget()
	global_position = pos
#	weaponSystem.Initialize()
	RestoreHealthToMaximum()

func GetTargetBot():
	return targetSystem.GetTarget()

func add_damage(damage : float, shooter) -> void:
	print("Bot ", name, " damaged by ", shooter.name)
	# just return if already dead or spawning
	if isDead() or isSpawning():
		print("Dead or Spawning. Return")
		return

	#the extra info field of the telegram carries the amount of damage
	ReduceHealth(damage)

	# if this bot is now dead let the shooter know
	if isDead():
		var add_frag : FuncRef = funcref(shooter, "add_frag")
	
		if add_frag.is_valid():
			add_frag.call_func()

func add_frag() -> void:
	IncrementScore()
	
	# the bot this bot has just killed should be removed as the target
	targetSystem.ClearTarget()
	
func die():
#	var dead_enemy : = DEAD_ENEMY.instance()
#	
#	dead_enemy.global_position = global_position
#	get_parent().add_child(dead_enemy)
#	
#	Events.emit_signal("enemy_destroyed")
#	
#	queue_free()
	pass

func hear_sound(sound_type : int, sound_source):
	if sound_source == self:
		return
		
	# add the source of this sound to the bot's percepts
	sensoryMem.UpdateWithSoundSource(sound_source)
	
#--------------------------- HandleMessage -----------------------------------
#-----------------------------------------------------------------------------
#bool Raven_Bot::HandleMessage(const Telegram& msg)
#{
#  //first see if the current goal accepts the message
#  if (GetBrain()->HandleMessage(msg)) return true;
# 
#  //handle any messages not handles by the goals
#  switch(msg.Msg)
#  {
#  case Msg_TakeThatMF:
#
#	//just return if already dead or spawning
#	if (isDead() || isSpawning()) return true;
#
#	//the extra info field of the telegram carries the amount of damage
#	ReduceHealth(DereferenceToType<int>(msg.ExtraInfo));
#
#	//if this bot is now dead let the shooter know
#	if (isDead())
#	{
#	  Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
#							  ID(),
#							  msg.Sender,
#							  Msg_YouGotMeYouSOB,
#							  NO_ADDITIONAL_INFO);
#	}
#
#	return true;
#
#  case Msg_YouGotMeYouSOB:
#	
#	IncrementScore();
#	
#	//the bot this bot has just killed should be removed as the target
#	m_pTargSys->ClearTarget();
#
#	return true;
#
#  case Msg_GunshotSound:
#
#	//add the source of this sound to the bot's percepts
#	GetSensoryMem()->UpdateWithSoundSource((Raven_Bot*)msg.ExtraInfo);
#
#	return true;
#
#  case Msg_UserHasRemovedBot:
#	{
#
#	  Raven_Bot* pRemovedBot = (Raven_Bot*)msg.ExtraInfo;
#
#	  GetSensoryMem()->RemoveBotFromMemory(pRemovedBot);
#
#	  //if the removed bot is the target, make sure the target is cleared
#	  if (pRemovedBot == GetTargetSys()->GetTarget())
#	  {
#		GetTargetSys()->ClearTarget();
#	  }
#
#	  return true;
#	}
#
#
#  default: return false;
#  }
#}
