extends GoalComposite
class_name GoalExplore

var currentDestination : Vector2

# set to true when the destination for the exploration has been established
var destinationIsSet : bool

func _init(bot_agent).(bot_agent, GoalType.Explore):
	destinationIsSet = false
	
#------------------------------ Activate -------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
	
	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()
	
	agent.pathPlanner.connect("PathReady", self, "On_PathReady")
	
	if not destinationIsSet:
		# grab a random location
		currentDestination = WorldController.GetRandomNodeLocation()
		
		destinationIsSet = true
	
	# and request a path to that position
	agent.pathPlanner.RequestPathToPosition(currentDestination)
	
	# the bot may have to wait a few update cycles before a path is calculated
	# so for appearances sake it simple ARRIVES at the destination until a path
	# has been found
	AddSubgoal(GoalSeekToPosition.new(agent, currentDestination))


#------------------------------ Process -------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	return status


func On_PathReady(extraInfo):
	#clear any existing goals
	RemoveAllSubgoals()
	AddSubgoal(GoalFollowPath.new(agent, agent.pathPlanner.GetPath()))
	
	
	
func On_NoPathAvailable():
	status = GoalStatus.Failed
#---------------------------- HandleMessage ----------------------------------
#-----------------------------------------------------------------------------
func HandleMessage(msg) -> bool:
	# first, pass the message down the goal hierarchy
	var handled : bool = ForwardMessageToFrontMostSubgoal(msg)
	
	# if the msg was not handled, test to see if this goal can handle it
	if handled == false:
		match msg.Msg:
			"Msg_PathReady":
				#clear any existing goals
				RemoveAllSubgoals()
				AddSubgoal(GoalFollowPath.new(agent, agent.pathPlanner.GetPath()))
				return true #msg handled
			
			
			"Msg_NoPathAvailable":
				status = GoalStatus.Failed
				return true #msg handled
			
			_:
				return false
	
	# handled by subgoals
	return true
