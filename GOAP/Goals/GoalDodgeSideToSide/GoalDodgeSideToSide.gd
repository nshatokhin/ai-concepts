extends Goal
class_name GoalDodgeSideToSide

#-----------------------------------------------------------------------------
#
#  This goal makes the bot dodge from side to side
#
#-----------------------------------------------------------------------------

var strafeTarget :Vector2

var clockwise : bool

var rng = RandomNumberGenerator.new()

func _init(bot_agent).(bot_agent, GoalType.Strafe):
	rng.randomize()
	clockwise = true if rng.randi() % 2 else false
	

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
	
	agent.steering.SeekOn()
  
	if clockwise:
		strafeTarget = agent.global_position + agent.facingSide * agent.boundingRadius()
		if agent.canStepRight(strafeTarget):
			agent.steering.target = strafeTarget
		else:
			clockwise = !clockwise;
			status = GoalStatus.Inactive
	else:
		strafeTarget = agent.global_position - agent.facingSide * agent.boundingRadius()
		if agent.canStepLeft(strafeTarget):
			agent.steering.target = strafeTarget
		else:
			clockwise = !clockwise
			status = GoalStatus.Inactive


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# if target goes out of view terminate
	if not agent.targetSystem.isTargetWithinFOV():
		status = GoalStatus.Completed
	
	# else if bot reaches the target position set status to inactive so the goal 
	# is reactivated on the next update-step
	elif agent.isAtPosition(strafeTarget):
		status = GoalStatus.Inactive
	
	return status


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	agent.steering.SeekOff()


#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem):
	var target_in_local = agent.global_transform.xform_inv(strafeTarget)
	canvas.draw_line(Vector2(), target_in_local, Color("#FF7F00"))
	canvas.draw_circle( target_in_local, 3.0, Color("#FF7F00") )


