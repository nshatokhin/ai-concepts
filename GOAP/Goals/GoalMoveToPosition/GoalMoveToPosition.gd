extends GoalComposite
class_name GoalMoveToPosition

var destination : Vector2

func _init(bot_agent, pos : Vector2).(bot_agent, GoalType.MoveToPosition):
	destination = pos
	
	agent.pathPlanner.connect("PathReady", self, "On_PathReady")
	agent.pathPlanner.connect("NoPathAvailable", self, "On_NoPathAvailable")

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active;
	
	# make sure the subgoal list is clear.
	RemoveAllSubgoals()
	
	# requests a path to the target position from the path planner. Because, for
	# demonstration purposes, the Raven path planner uses time-slicing when 
	# processing the path requests the bot may have to wait a few update cycles
	# before a path is calculated. Consequently, for appearances sake, it just
	# seeks directly to the target position whilst it's awaiting notification
	# that the path planning request has succeeded/failed
	if agent.pathPlanner.RequestPathToPosition(destination):
		AddSubgoal(GoalSeekToPosition.new(agent, destination))

#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	# if any of the subgoals have failed then this goal re-plans
	_reactivateIfFailed()
	
	return status

func Terminate() -> void:
	status = GoalStatus.Completed

func On_PathReady(extraInfo):
	# clear any existing goals
	RemoveAllSubgoals();
	AddSubgoal(GoalFollowPath.new(agent, agent.pathPlanner.GetPath()))
	
	
func On_NoPathAvailable():
	status = GoalStatus.Failed
	
#---------------------------- HandleMessage ----------------------------------
#-----------------------------------------------------------------------------
func HandleMessage(msg) -> bool:
	# first, pass the message down the goal hierarchy
	var handled : bool = ForwardMessageToFrontMostSubgoal(msg)
	
	# if the msg was not handled, test to see if this goal can handle it
	if handled == false:
		match msg.Msg:
			"Msg_PathReady":
			
				# clear any existing goals
				RemoveAllSubgoals();
				
				AddSubgoal(GoalFollowPath.new(agent, agent.pathPlanner.GetPath()))
				
				return true # msg handled
			
			
			"Msg_NoPathAvailable":
			
				status = GoalStatus.Failed
				
				return true # msg handled
			
			_:
				return false
	
	#handled by subgoals
	return true

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	# forward the request to the subgoals
	.draw(canvas)
	
	# draw a bullseye
	var destination_in_local = agent.global_transform.xform_inv(destination)
	canvas.draw_circle(destination_in_local, 6.0, Color("#0000ff"))
	canvas.draw_circle(destination_in_local, 4.0, Color("#ff0000"))
	canvas.draw_circle(destination_in_local, 2.0, Color("#00ffff"));
