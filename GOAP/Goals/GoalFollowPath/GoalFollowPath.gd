extends GoalComposite
class_name GoalFollowPath

#a local copy of the path returned by the path planner
var path : Array = []

#------------------------------ ctor -----------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, pth : Array).(bot_agent, GoalType.FollowPath):
	path = pth


#------------------------------ Activate -------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	if path.empty():
		status = GoalStatus.Failed
		return
		
	status = GoalStatus.Active
	
	# get a reference to the next edge
	var edge : PathEdge = path.front()
	
	# remove the edge from the path
	path.pop_front() 
	
	# some edges specify that the bot should use a specific behavior when
	# following them. This switch statement queries the edge behavior flag and
	# adds the appropriate goals/s to the subgoal list.
	match edge.behavior:
		NavGraphEdge.Normal:
			AddSubgoal(GoalTraverseEdge.new(agent, edge, path.empty()))
			
		
		NavGraphEdge.GoesThroughDoor:
			# also add a goal that is able to handle opening the door
#			AddSubgoal(GoalNegotiateDoor.new(agent, edge, path.empty()))
			pass
		
		NavGraphEdge.Jump:
			# add subgoal to jump along the edge
			pass
		
		NavGraphEdge.Grapple:
			# add subgoal to grapple along the edge
			pass
		
		_:
			print("<GoalFollowPath::Activate>: Unrecognized edge type")


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	status = ProcessSubgoals()
	
	# if there are no subgoals present check to see if the path still has edges.
	# remaining. If it does then call activate to grab the next edge.
	if status == Goal.GoalStatus.Completed and not path.empty():
		Activate()
		
	return status
	

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	# render all the path waypoints remaining on the path list
	for segment in path:
		var source_in_local = agent.global_transform.xform_inv(segment.source)
		var destination_in_local = agent.global_transform.xform_inv(segment.destination)
		canvas.draw_line(source_in_local, destination_in_local, Color("#000000"))
		canvas.draw_circle(destination_in_local, 3.0, Color("#ff0000"))
	
	# forward the request to the subgoals
	.draw(canvas)
 
