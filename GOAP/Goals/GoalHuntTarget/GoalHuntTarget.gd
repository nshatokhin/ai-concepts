extends GoalComposite
class_name GoalHuntTarget

#-----------------------------------------------------------------------------
#
#   Causes a bot to search for its current target. Exits when target
#          is in view
#-----------------------------------------------------------------------------

# this value is set to true if the last visible position of the target
# bot has been searched without success
var LVPTried : bool

func _init(bot_agent).(bot_agent, GoalType.HuntTarget):
	LVPTried = false

#---------------------------- Initialize -------------------------------------
#-----------------------------------------------------------------------------  
func Activate() -> void:
	status = GoalStatus.Active
	
	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()
	
	# it is possible for the target to die whilst this goal is active so we
	# must test to make sure the bot always has an active target
	if agent.targetSystem.isTargetPresent():
		# grab a local copy of the last recorded position (LRP) of the target
		var lrp : Vector2 = agent.targetSystem.GetLastRecordedPosition()
		
		# if the bot has reached the LRP and it still hasn't found the target
		# it starts to search by using the explore goal to move to random
		# map locations
		if lrp.length() <= Constants.Epsilon or agent.isAtPosition(lrp):
			AddSubgoal(GoalExplore.new(agent))
		# else move to the LRP
		else:
			AddSubgoal(GoalMoveToPosition.new(agent, lrp))
	
	# if their is no active target then this goal can be removed from the queue
	else:
		status = GoalStatus.Completed


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	status = ProcessSubgoals()
	
	# if target is in view this goal is satisfied
	if agent.targetSystem.isTargetWithinFOV():
		status = GoalStatus.Completed
	
	return status


#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	# render last recorded position as a green circle
	if agent.targetSystem.isTargetPresent():
		var lrp_in_local = agent.global_transform.xform_inv(agent.targetSystem.GetLastRecordedPosition())
		canvas.draw_circle(lrp_in_local, 3.0, Color("#ff0000"))
	
	# forward the request to the subgoals
	.draw(canvas)
 
