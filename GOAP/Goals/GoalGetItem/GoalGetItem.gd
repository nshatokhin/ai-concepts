extends GoalComposite
class_name GoalGetItem

var itemToGet : int

var giverTrigger

# true if a path to the item has been formulated
var followingPath : bool

# returns true if the bot sees that the item it is heading for has been
# picked up by an opponent
func hasItemBeenStolen() -> bool:
	return (giverTrigger and not giverTrigger.isActive() and agent.hasLOSto(giverTrigger.global_position))


func _init(bot_agent, item : int).(bot_agent, get_script().ItemTypeToGoalType(item)):
	itemToGet = item
	giverTrigger = null
	followingPath = false
	
	agent.pathPlanner.connect("PathReady", self, "On_PathReady")
	agent.pathPlanner.connect("NoPathAvailable", self, "On_NoPathAvailable")

func Activate() -> void:
	status = GoalStatus.Active
	
	giverTrigger = null
	
	# request a path to the item
	agent.pathPlanner.RequestPathToItem(itemToGet)
	
	# the bot may have to wait a few update cycles before a path is calculated
	# so for appearances sake it just wanders
	AddSubgoal(GoalWander.new(agent))


func Process() -> int:
	_activateIfInactive()
	
	if hasItemBeenStolen():
		Terminate()
	else:
		# process the subgoals
		status = ProcessSubgoals()
	
	return status

func Terminate() -> void:
	status = GoalStatus.Completed

# helper function to change an item type enumeration into a goal type
static func ItemTypeToGoalType(gt : int) -> int:
	match gt:
		TriggerItemGiver.ItemType.Health:
			return Goal.GoalType.GetHealth
		TriggerItemGiver.ItemType.Shotgun:
			return Goal.GoalType.GetShotgun
		TriggerItemGiver.ItemType.RailGun:
			return Goal.GoalType.GetRailgun
		TriggerItemGiver.ItemType.RocketLauncher:
			return Goal.GoalType.GetRocketLauncher
		_:
			print("Goal_GetItem cannot determine item type")
			return 0

func On_PathReady(extraInfo):
	# clear any existing goals
	RemoveAllSubgoals()
	
	AddSubgoal(GoalFollowPath.new(agent, agent.pathPlanner.GetPath()))
	
	# get the pointer to the item
	giverTrigger = extraInfo
	
	
func On_NoPathAvailable():
	status = GoalStatus.Failed
