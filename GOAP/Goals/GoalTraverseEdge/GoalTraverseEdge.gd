extends Goal
class_name GoalTraverseEdge

# the edge the bot will follow
var edge : PathEdge

# true if m_Edge is the last in the path.
var lastEdgeInPath : bool

# the estimated time the bot should take to traverse the edge
var timeExpected : float
  
# this records the time this goal was activated
var startTime : float

#---------------------------- ctor -------------------------------------------
#-----------------------------------------------------------------------------
func _init(bot_agent, Edge : PathEdge, LastEdge : bool).(bot_agent, GoalType.TraverseEdge):
	edge = Edge
	timeExpected = 0.0
	lastEdgeInPath = LastEdge


#---------------------------- Activate -------------------------------------
#-----------------------------------------------------------------------------
# factor in a margin of error for any reactive behavior
var MarginOfError : float = 2.0
func Activate() -> void:
	status = GoalStatus.Active
	
	# the edge behavior flag may specify a type of movement that necessitates a 
	# change in the bot's max possible speed as it follows this edge
	match edge.behavior:
		NavGraphEdge.Swim:
			agent.maxSpeed = agent.MaxSwimmingSpeed
		
		NavGraphEdge.Crawl:
			agent.maxSpeed = agent.MaxCrawlingSpeed
			pass
	
	
	# record the time the bot starts this goal
	startTime = Clock.GetCurrentTime()
	
	# calculate the expected time required to reach the this waypoint. This value
	# is used to determine if the bot becomes stuck 
	timeExpected = agent.CalculateTimeToReachPosition(edge.destination)
	
	timeExpected += MarginOfError

	# set the steering target
	agent.steering.target = edge.destination
	
	# Set the appropriate steering behavior. If this is the last edge in the path
	# the bot should arrive at the position it points to, else it should seek
	if lastEdgeInPath:
		agent.steering.ArriveOn()
	else:
		agent.steering.SeekOn()


#------------------------------ Process --------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# if the bot has become stuck return failure
	if isStuck():
		status = GoalStatus.Failed
	# if the bot has reached the end of the edge return completed
	else:
		if agent.isAtPosition(edge.destination):
			status = GoalStatus.Completed
	
	return status

#--------------------------- isBotStuck --------------------------------------
#
#  returns true if the bot has taken longer than expected to reach the 
#  currently active waypoint
#-----------------------------------------------------------------------------
func isStuck() -> bool:
	var TimeTaken : float = Clock.GetCurrentTime() - startTime;
	
	if TimeTaken > timeExpected:
		print("traverse edge, BOT ", agent, " IS STUCK!!")
		return true
	
	return false


#---------------------------- Terminate --------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	# turn off steering behaviors.
	agent.steering.SeekOff()
	agent.steering.ArriveOff()
	
	# return max speed back to normal
	agent.maxSpeed = agent.MaxSpeed
	

#-------------------------------- Draw ---------------------------------------
#-----------------------------------------------------------------------------
func draw(canvas : CanvasItem) -> void:
	if status == GoalStatus.Active:
		var destination_in_local = agent.global_transform.xform_inv(edge.destination)
		canvas.draw_line(Vector2(), destination_in_local, Color("#0000ff"))
		canvas.draw_circle( destination_in_local, 3.0, Color("#00ff00"))
