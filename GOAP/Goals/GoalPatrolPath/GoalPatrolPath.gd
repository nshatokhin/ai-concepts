extends GoalComposite
class_name GoalPatrolPath

var currentPath : PoolVector2Array

# set to true when the destination for the exploration has been established
var destinationIsSet : bool

func _init(bot_agent, path : PoolVector2Array).(bot_agent, GoalType.PatrolPath):
	destinationIsSet = false
	currentPath = path
	
#------------------------------ Activate -------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active
	
	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()
	
	for point in currentPath:
		AddSubgoal(GoalMoveToPosition.new(agent, point))


#------------------------------ Process -------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# if status is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	return status

