extends GoalComposite
class_name GoalAttackTarget

func _init(bot_agent).(bot_agent, GoalType.AttackTarget):
	pass

#------------------------------- Activate ------------------------------------
#-----------------------------------------------------------------------------
func Activate() -> void:
	status = GoalStatus.Active

	# if this goal is reactivated then there may be some existing subgoals that
	# must be removed
	RemoveAllSubgoals()

	# it is possible for a bot's target to die whilst this goal is active so we
	# must test to make sure the bot always has an active target
	if not agent.targetSystem.isTargetPresent():
		status = GoalStatus.Completed
		
		return

	# if the bot is able to shoot the target (there is LOS between bot and
	# target), then select a tactic to follow while shooting
	if agent.targetSystem.isTargetShootable():
		# if the bot has space to strafe then do so
		var dummy :Vector2
		if agent.canStepLeft(dummy) or agent.canStepRight(dummy):
			AddSubgoal(GoalDodgeSideToSide.new(agent))

		# if not able to strafe, head directly at the target's position 
		else:
			AddSubgoal(GoalSeekToPosition.new(agent, agent.GetTargetBot().global_position))

	# if the target is not visible, go hunt it.
	else:
		AddSubgoal(GoalHuntTarget.new(agent))


#-------------------------- Process ------------------------------------------
#-----------------------------------------------------------------------------
func Process() -> int:
	# rocesstus is inactive, call Activate()
	_activateIfInactive()
	
	# process the subgoals
	status = ProcessSubgoals()
	
	_reactivateIfFailed()
	
	return status


#-------------------------- Terminate ------------------------------------------
#-----------------------------------------------------------------------------
func Terminate() -> void:
	status = GoalStatus.Completed
   
