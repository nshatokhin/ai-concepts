extends GoalEvaluator
class_name GetWeaponGoalEvaluator

var weaponType : int

func _init(bias : float, WeaponType : int).(bias):
	weaponType = WeaponType
	

#------------------- CalculateDesirability ---------------------------------
#-----------------------------------------------------------------------------
func CalculateDesirability(agent):
	# grab the distance to the closest instance of the weapon type
	var Distance : float = Feature.DistanceToItem(agent, weaponType)

	# if the distance feature is rated with a value of 1 it means that the
	# item is either not present on the map or too far away to be worth 
	# considering, therefore the desirability is zero
	if Distance == 1:
		return 0
	else:
		# value used to tweak the desirability
		var Tweaker : float = 0.15

		var Health : float = Feature.Health(agent)
		var WeaponStrength : float = Feature.IndividualWeaponStrength(agent, weaponType)
	
		var Desirability : float = (Tweaker * Health * (1-WeaponStrength)) / Distance

		# ensure the value is in the range 0 to 1
		clamp(Desirability, 0, 1)

		Desirability *= characterBias

		return Desirability


#------------------------------ SetGoal --------------------------------------
func SetGoal(agent) -> void:
	agent.brain.AddGoal_GetItem(weaponToItem(weaponType))
	
func weaponToItem(weapon : int) -> int:
	match weapon:
		Weapon.WeaponType.Shotgun:
			return TriggerItemGiver.ItemType.Shotgun
		Weapon.WeaponType.RailGun:
			return TriggerItemGiver.ItemType.RailGun
		Weapon.WeaponType.RocketLauncher:
			return TriggerItemGiver.ItemType.RocketLauncher
		_:
			print("<GetWeaponGoalEvaluator>: unknown weapon ", weapon)
			return 0

func GetInfo(agent) -> String:
	return String("GetWeaponGoal: ") + String(CalculateDesirability(agent))
