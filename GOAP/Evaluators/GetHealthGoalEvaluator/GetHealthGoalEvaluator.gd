extends GoalEvaluator
class_name GetHealthGoalEvaluator


func _init(bias : float).(bias):
	pass
	

#---------------------- CalculateDesirability -------------------------------------
#-----------------------------------------------------------------------------
func CalculateDesirability(agent) -> float:
	# first grab the distance to the closest instance of a health item
	var Distance : float = Feature.DistanceToItem(agent, TriggerItemGiver.ItemType.Health)

	# if the distance feature is rated with a value of 1 it means that the
	# item is either not present on the map or too far away to be worth 
	# considering, therefore the desirability is zero
	var Desirability : float = 0.0
	if Distance == 1.0:
		return Desirability
	else:
		# value used to tweak the desirability
		var Tweaker : float = 0.2
  
		# the desirability of finding a health item is proportional to the amount
		# of health remaining and inversely proportional to the distance from the
		# nearest instance of a health item.
		Desirability = Tweaker * ((1.0 - Feature.Health(agent)) / Distance)
 
		# ensure the value is in the range 0 to 1
		clamp(Desirability, 0, 1);
  
		# bias the value according to the personality of the bot
		Desirability *= characterBias

	return Desirability


#----------------------------- SetGoal ---------------------------------------
#-----------------------------------------------------------------------------
func SetGoal(agent) -> void:
	agent.brain.AddGoal_GetItem(TriggerItemGiver.ItemType.Health) 

func GetInfo(agent) -> String:
	return String("GetHealthGoal: ") + String(CalculateDesirability(agent))
