extends Goal
class_name GoalComposite


var subgoals : = []

func _init(bot_agent, t : int).(bot_agent, t):
	pass

#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		RemoveAllSubgoals()

func ProcessSubgoals() -> int:
	while !subgoals.empty() and (subgoals[0].isComplete() or subgoals[0].hasFailed()):
		subgoals[0].Terminate();
		#subgoals[0].free()
		subgoals.pop_front();

	if !subgoals.empty():
		var StatusOfSubGoals : int = subgoals[0].Process();
		
		if StatusOfSubGoals == GoalStatus.Completed and subgoals.size() > 1:
			return GoalStatus.Active
		
		return StatusOfSubGoals
	else:
		return GoalStatus.Completed

func ForwardMessageToFrontMostSubgoal(msg) -> bool:
	if !subgoals.empty():
		return subgoals[0].HandleMessage(msg)
	return false

func Activate() -> void:
	pass

func Process() -> int:
	return GoalStatus.Inactive

func Terminate() -> void:
	pass

func HandleMessage(msg) -> bool:
	return ForwardMessageToFrontMostSubgoal(msg)

func AddSubgoal(g : Goal) -> void:
	subgoals.push_front(g)

func RemoveAllSubgoals() -> void:
	for subgoal in subgoals:
		subgoal.Terminate()
		#subgoal.free()
	
	subgoals.clear()

func getGoalsText() -> String:
	var text : String = .getGoalsText()
	
	for subgoal in subgoals:
		text +="\n-"
		text += subgoal.getGoalsText()
	
	return text
	
func draw(canvas : CanvasItem):
	if not subgoals.empty():
		subgoals.front().draw(canvas)
