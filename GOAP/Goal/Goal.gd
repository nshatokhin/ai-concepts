extends Reference
class_name Goal

enum GoalStatus {
	Active = 0
	Inactive = 1
	Completed = 2
	Failed = 3
}

enum GoalType {
	Think
	Explore
	ArriveAtPosition
	SeekToPosition
	FollowPath
	TraverseEdge
	MoveToPosition
	GetHealth
	GetShotgun
	GetRocketLauncher
	GetRailgun
	Wander
	NegotiateDoor
	AttackTarget
	HuntTarget
	Strafe
	AdjustRange
	SayPhrase
	PatrolPath
}

func ConvertToString(gt : int) -> String:
	match gt:
		GoalType.Explore:
			return "Explore";
		
		GoalType.Think:
			return "Think";
		
		GoalType.ArriveAtPosition:
			return "ArriveAtPosition";
		
		GoalType.SeekToPosition:
			return "SeekToPosition";
		
		GoalType.FollowPath:
			return "FollowPath";
		
		GoalType.TraverseEdge:
			return "TraverseEdge";
		
		GoalType.MoveToPosition:
			return "MoveToPosition";
		
		GoalType.GetHealth:
			return "GetHealth";
		
		GoalType.GetShotgun:
			return "GetShotgun";
		
		GoalType.GetRailgun:
			return "GetRailgun";
		
		GoalType.GetRocketLauncher:
			return "GetRocketLauncher";
		
		GoalType.Wander:
			return "Wander";
		
		GoalType.NegotiateDoor:
			return "NegotiateDoor";
		
		GoalType.AttackTarget:
			return "AttackTarget";
		
		GoalType.HuntTarget:
			return "HuntTarget";
		
		GoalType.Strafe:
			return "Strafe";
		
		GoalType.AdjustRange:
			return "AdjustRange";
		
		GoalType.SayPhrase:
			return "SayPhrase";
			
		GoalType.PatrolPath:
			return "PatrolPath";
		
		_:
			return "UNKNOWN GOAL TYPE!";


export (int) var type
export (int) var status = GoalStatus.Inactive

var agent = null

func _init(bot_agent, goal_type : int):
	agent = bot_agent
	type = goal_type
	
#func _notification(what):
#	if what == NOTIFICATION_PREDELETE:
#		pass

func _activateIfInactive():
	if isInactive():
		Activate()

func _reactivateIfFailed():
	if hasFailed():
		status = GoalStatus.Inactive
	
func Activate() -> void:
	pass

func Process() -> int:
	return status

func Terminate() -> void:
	pass

func HandleMessage(msg) -> bool:
	return false

func AddSubgoal(g : Goal) -> void:
	print("Cannot add goals to atomic goals")


func isComplete() -> bool:
	return status == GoalStatus.Completed 
func isActive() -> bool:
	return status == GoalStatus.Active
func isInactive() -> bool:
	return status == GoalStatus.Inactive
func hasFailed() -> bool:
	return status == GoalStatus.Failed
func GetType() -> int:
	return type

func getGoalsText() -> String:
	var text : String = ""
	
	if isComplete():
		text += "[C] "
	if isInactive():
		text += "[I] "
	if hasFailed():
		text += "[F] "
	if isActive():
		text += "[A] "

	text += ConvertToString(type)
	
	return text

func draw(canvas: CanvasItem):
	pass
