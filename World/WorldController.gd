extends Node

onready var rng = RandomNumberGenerator.new()

onready var pathManager = PathManager.new(UserOptions.MaxSearchCyclesPerUpdateStep)

var graph : SparseGraph setget set_graph

var pathCosts : Array = []

# the size of the search radius the cellspace partition uses when looking for 
# neighbors 
var cellSpaceNeighborhoodRange : float

var triggers : Array = []

onready var selectedBot = null

func _ready():
	rng.randomize()

func _process(delta):
	pathManager.UpdateSearches()

func GetAllBots() -> Array:
	return []

#func GetRandomNodeLocation() -> Vector2:
#	var screen_width = ProjectSettings.get_setting("display/window/size/width")
#	var screen_height = ProjectSettings.get_setting("display/window/size/height")
#	return Vector2(rng.randi_range(- screen_width / 2, screen_width / 2), rng.randi_range(-screen_height/2, screen_height/2))


#  returns the position of a graph node selected at random
func GetRandomNodeLocation() -> Vector2:
	var RandIndex : int = rng.randi_range(0, graph.NumActiveNodes() - 1)
	var node = graph.GetValidNodeByNum(RandIndex)
	
	if node:
		return node.position
	else:
		return Vector2()

func FloodFillGraph(position : Vector2) -> void:
	pass

func set_graph(g : SparseGraph):
	graph = g
	
	# calculate the cost lookup table
	pathCosts = GraphUtils.CreateAllPairsCostsTable(graph)

#------------- CalculateCostToTravelBetweenNodes -----------------------------
#
#  Uses the pre-calculated lookup table to determine the cost of traveling
#  from nd1 to nd2
#-----------------------------------------------------------------------------
func CalculateCostToTravelBetweenNodes(nd1 : int, nd2 : int) -> float:
	if not (nd1 >= 0 and nd1 < graph.NumNodes() and nd2 >= 0 and nd2 < graph.NumNodes()):
		print("<WorldController.CostBetweenNodes>: invalid index")
		return 0.0

	return pathCosts[nd1][nd2]
