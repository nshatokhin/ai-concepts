extends Reference
class_name IndexedPriorityQLow

#----------------------- IndexedPriorityQLow ---------------------------
#
#   Priority queue based on an index into a set of keys. The queue is
#   maintained as a 2-way heap.
#
#   The priority in this implementation is the lowest valued key
#------------------------------------------------------------------------

var vecKeys : Array = []

var heap : Array = []
 
var invHeap : Array = []

var size : int
var maxSize : int

func Swap(a : int, b : int) -> void:
	var temp = heap[a]
	heap[a] = heap[b]
	heap[b] = temp

	# change the handles too
	invHeap[heap[a]] = a
	invHeap[heap[b]] = b

func ReorderUpwards(nd : int) -> void:
	# move up the heap swapping the elements until the heap is ordered
	while nd > 1 and vecKeys[heap[nd/2]] > vecKeys[heap[nd]]:
		Swap(nd/2, nd)
		
		nd /= 2


func ReorderDownwards(nd : int, HeapSize : int) -> void:
	# move down the heap from node nd swapping the elements until
	# the heap is reordered
	while 2*nd <= HeapSize:
		var child : int = 2 * nd
		
		# set child to smaller of nd's two children
		if child < HeapSize and vecKeys[heap[child]] > vecKeys[heap[child+1]]:
			child += 1
		
		# if this nd is larger than its child, swap
		if vecKeys[heap[nd]] > vecKeys[heap[child]]:
			Swap(child, nd)
			
			# move the current node down the tree
			nd = child
		else:
			break

# you must pass the constructor a reference to the array the PQ
# will be indexing into and the maximum size of the queue.
func _init(keys : Array, MaxSize : int):
	vecKeys = keys
	maxSize = MaxSize
	size = 0
	heap.resize(MaxSize+1)
	invHeap.resize(MaxSize+1)
	
func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		heap.clear()
		invHeap.clear()

func empty() -> bool:
	return (size == 0)

# to insert an item into the queue it gets added to the end of the heap
# and then the heap is reordered from the bottom up.
func insert(idx : int) -> void:
	if not (size+1 <= maxSize):
		print("<IndexedPriorityQLow>: queue is full")
		return
	
	size += 1
	
	heap[size] = idx
	
	invHeap[idx] = size;
	
	ReorderUpwards(size)

# to get the min item the first element is exchanged with the lowest
# in the heap and then the heap is reordered from the top down. 
func Pop() -> int:
	Swap(1, size)
	
	ReorderDownwards(1, size-1)
	
	var item = heap[size]
	size -= 1
	return item


# if the value of one of the client key's changes then call this with 
# the key's index to adjust the queue accordingly
func ChangePriority(idx : int) -> void:
	ReorderUpwards(invHeap[idx])
