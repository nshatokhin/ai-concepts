extends Reference
class_name PriorityQ

#--------------------- PriorityQ ----------------------------------------
#
#  basic heap based priority queue implementation
#------------------------------------------------------------------------

var heap : Array = []

var size : int

var maxSize : int

func Swap(a : int, b : int) -> void:
	var temp = heap[a]
	heap[a] = heap[b]
	heap[b] = temp
	
# given a heap and a node in the heap, this function moves upwards
# through the heap swapping elements until the heap is ordered
func ReorderUpwards(nd : int) -> void:
	# move up the heap swapping the elements until the heap is ordered
	while nd > 1 and heap[nd/2] < heap[nd]:
		Swap(nd/2, nd)
		
		nd /= 2

# given a heap, the heapsize and a node in the heap, this function
# reorders the elements in a top down fashion by moving down the heap
# and swapping the current node with the greater of its two children
# (provided a child is larger than the current node)
func ReorderDownwards(nd : int, HeapSize : int) -> void:
	# move down the heap from node nd swapping the elements until
	# the heap is reordered
	while 2*nd <= HeapSize:
		var child : int = 2 * nd
		
		# set child to largest of nd's two children
		if child < HeapSize and heap[child] < heap[child+1]:
			child += 1
		
		# if this nd is smaller than its child, swap
		if heap[nd] < heap[child]:
			Swap(child, nd)
			
			# move the current node down the tree
			nd = child
		else:
			break


func _init(MaxSize : int):
	maxSize = MaxSize
	size = 0
	heap.resize(MaxSize+1)


func empty() -> bool:
	return size == 0

# to insert an item into the queue it gets added to the end of the heap
# and then the heap is reordered
func insert(item) -> void:
	if not (size + 1 <= maxSize):
		print("<PriorityQ>: queue is full")
		return
	
	size += 1
	
	heap[size] = item
	
	ReorderUpwards(size)

# to get the max item the first element is exchanged with the lowest
# in the heap and then the heap is reordered from the top down. 
func pop():
	Swap(1, size)
	
	ReorderDownwards(1, size - 1)
	
	var item = heap[size]
	size -= 1
	
	return item

# so we can take a peek at the first in line
func Peek():
	return heap[1]
