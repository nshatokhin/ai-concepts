extends TriggerItemGiver
class_name TriggerWeaponGiver

# the amount of health an entity receives when it runs over this trigger
export (Weapon.WeaponType) var weapon : int = Weapon.WeaponType.Blaster

func _init().():
	itemType = weapon
	
func _ready():
	addTriggerRegion(self, "_on_body_entered")

func _on_body_entered(body):
	if isActive():
		var weapon_func : FuncRef = funcref(body, "AddWeapon")
		
		if weapon_func.is_valid():
			weapon_func.call_func(weapon)
			Deactivate()
