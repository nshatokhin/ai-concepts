extends TriggerRegion
class_name TriggerRegionCircle

var _radius : float = 0.0

func _init(pos : Vector2, radius : float).():
	_radius = radius
	origin = pos
	
	var shape : = CircleShape2D.new()
	shape.set_radius(_radius)
	var collision_shape : = CollisionShape2D.new()
	collision_shape.set_shape(shape)
	
	add_child(collision_shape)
	
func draw(canvas : CanvasItem):
	canvas.draw_circle( Vector2(), _radius, Color("#400000ff"))
	
