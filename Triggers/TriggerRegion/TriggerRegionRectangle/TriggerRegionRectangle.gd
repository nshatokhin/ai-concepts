extends TriggerRegion
class_name TriggerRegionRectangle

func _init(TopLeft : Vector2, BottomRight : Vector2).():
	var shape : = RectangleShape2D.new()
	shape.set_extents(Vector2((BottomRight.x - TopLeft.x) / 2, (BottomRight.y - TopLeft.y) / 2))
	var collision_shape : = CollisionShape2D.new()
	collision_shape.set_shape(shape)
	
	add_child(collision_shape)
	
func draw(canvas : CanvasItem):
	pass
