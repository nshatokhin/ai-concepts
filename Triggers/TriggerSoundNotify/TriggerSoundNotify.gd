extends TriggerLimitedLifetime
class_name TriggerSoundNotify

enum SoundType {
	GunShoot
}

var soundSource = null
var boundingRadius : float = 0.0
var soundType : int = SoundType.GunShoot

func _init(source, distance : float, type : int).(UserOptions.TriggerTestRegulatorTime):
	soundSource = source
	soundType = type
	# set position and range
	origin = soundSource.global_position
	
	boundingRadius = distance

func _ready():
	global_position = origin
	# create and set this trigger's region of fluence
	AddCircularTriggerRegion(origin, boundingRadius, self, "_on_body_entered")

func _on_body_entered(body):
	var sound_func : FuncRef = funcref(body, "hear_sound")
	
	if sound_func.is_valid():
		sound_func.call_func(soundType, soundSource)
