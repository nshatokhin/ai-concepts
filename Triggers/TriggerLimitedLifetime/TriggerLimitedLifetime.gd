extends Trigger
class_name TriggerLimitedLifetime

export (float) var _lifetime : float = 0.0
var timer : Timer

func _init(lifetime : float = 0.0).():
	_lifetime = lifetime
	timer = Timer.new()
	timer.connect("timeout", self, "queue_free")
	add_child(timer)

func _ready():
	timer.set_wait_time(_lifetime)
	timer.start()
