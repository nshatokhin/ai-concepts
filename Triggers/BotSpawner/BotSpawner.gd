extends Position2D

var bot_scene : = preload("res://Characters/Bot/Bot.tscn")

func SpawnBot(path : PoolVector2Array = PoolVector2Array()):
	var bot = bot_scene.instance()
	
	bot.patrolPath = path
	
	get_parent().add_child(bot)
	
	bot.steering.SeparationOn()
#	bot.steering.WallAvoidanceOn()
	bot.Spawn(self.global_position)
