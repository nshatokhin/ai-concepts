extends TriggerRespawning
class_name TriggerItemGiver

enum ItemType {
	Health
	Shotgun
	RailGun
	RocketLauncher
}

var boundingRadius : float = 0.0

export (float) var triggerRespawnDelay : float = 10.0
export (float) var triggerRange : float = 30.0

onready var sprite : = $AnimatedSprite

var itemType : int = -1

func _init().():
	triggerType = Type.Giver
	boundingRadius = triggerRange
	
	SetRespawnDelay(triggerRespawnDelay);
	
func addTriggerRegion(receiver, receiver_func : String):
	# create and set this trigger's region of fluence
	AddCircularTriggerRegion(global_position, boundingRadius, receiver, receiver_func)
	
func _process(delta):
	sprite.visible = isActive()
