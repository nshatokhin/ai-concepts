extends TriggerLimitedLifetime
class_name TriggerExplosion

enum SoundType {
	GunShoot
}

var explosionSource = null
var boundingRadius : float = 0.0
var damageInflicted : float = 0.0

func _init(source, Origin : Vector2, distance : float, damage : float).(UserOptions.TriggerExplosionTime):
	explosionSource = source
	damageInflicted = damage
	# set position and range
	origin = Origin
	
	boundingRadius = distance
	
func _ready():
	global_position = origin
	# create and set this trigger's region of fluence
	AddCircularTriggerRegion(origin, boundingRadius, self, "_on_body_entered")
	
	# TODO: add explosion sound
	# AddSound(agent, Params.SoundRange)

func _on_body_entered(body):
	var space_state : Physics2DDirectSpaceState = get_world_2d().direct_space_state
	var intersection : Dictionary = space_state.intersect_ray(global_position, body.global_position)
	
	if not intersection.empty() and intersection.collider == body:
		var damage_func : FuncRef = funcref(body, "add_damage")
	
		if damage_func.is_valid():
			damage_func.call_func(damageInflicted * (1.0 - (global_position - body.global_position).length() / boundingRadius), explosionSource)
