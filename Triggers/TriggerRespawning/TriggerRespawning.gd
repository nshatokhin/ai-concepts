extends Trigger
class_name TriggerRespawning

var timer : Timer

# When a bot comes within this trigger's area of influence it is triggered
# but then becomes inactive for a specified amount of time. These values
# control the amount of time required to pass before the trigger becomes 
# active once more.
var respawnDelay : int

# sets the trigger to be inactive for respawnDelay 
# seconds
func Deactivate() -> void:
	SetInactive()
	timer.set_wait_time(respawnDelay)
	timer.start()

func _init().():
	respawnDelay = 0
	timer = Timer.new()
	timer.connect("timeout", self, "timeout")
	add_child(timer)

func timeout() -> void:
	if not isActive():
		SetActive()

func SetRespawnDelay(delay : int) -> void:
	respawnDelay = delay
