extends Position2D
class_name Trigger

#-----------------------------------------------------------------------------
#
#     Base class for a trigger. A trigger is an object that is
#     activated when an entity moves within its region of influence.
#
#-----------------------------------------------------------------------------

enum Type {
	Giver
}

var origin : Vector2

var triggerType : int

# entity trigger owns a trigger region. If an entity comes within this 
# region the trigger is activated
var regionOfInfluence : TriggerRegion # Area2D

# if this is true the trigger will be removed from the game
var removeFromGame : bool

# it's convenient to be able to deactivate certain types of triggers
# on an event. Therefore a trigger can only be triggered when this
# value is true (respawning triggers make good use of this facility)
var active : bool

# some types of trigger are twinned with a graph node. This enables
# the pathfinding component of an AI to search a navgraph for a specific
# type of trigger.
var graphNodeIndex : int

func SetGraphNodeIndex(idx : int) -> void:
	graphNodeIndex = idx

func SetToBeRemovedFromGame() -> void:
	removeFromGame = true
	
func SetInactive() -> void:
	active = false
	
func SetActive() -> void:
	active = true

# returns true if the entity given by a position and bounding radius is
# overlapping the trigger region
func isTouchingTrigger(EntityPos : Vector2, EntityRadius : float) -> bool:
	if regionOfInfluence:
		return regionOfInfluence.isTouching(EntityPos, EntityRadius)
	
	return false

# child classes use one of these methods to initialize the trigger region
func AddCircularTriggerRegion(center : Vector2, radius : float, signal_receiver, signal_slot : String) -> void:
	# if this replaces an existing region, tidy up memory
	if regionOfInfluence:
		remove_child(regionOfInfluence)
		regionOfInfluence = null
	
	regionOfInfluence = TriggerRegionCircle.new(center, radius)
	
	regionOfInfluence.connect("body_entered", signal_receiver, signal_slot)
	
	add_child(regionOfInfluence)

func AddRectangularTriggerRegion(TopLeft : Vector2, BottomRight : Vector2) -> void:
	# if this replaces an existing region, tidy up memory
	if regionOfInfluence:
		remove_child(regionOfInfluence)
		regionOfInfluence = null
	
	regionOfInfluence = TriggerRegionRectangle.new(TopLeft, BottomRight)
	
	add_child(regionOfInfluence)
	
func _init():
	removeFromGame = false
	active = true
	graphNodeIndex = -1
	regionOfInfluence = null

# when this is called the trigger determines if the entity is within the
# trigger's region of influence. If it is then the trigger will be 
# triggered and the appropriate action will be taken.
func Try(entity_type) -> void:
	pass

# called each update-step of the game. This methods updates any internal
# state the trigger may have
func Update() -> void:
	pass

func GraphNodeIndex() -> int:
	return graphNodeIndex
func isToBeRemoved() -> bool:
	return removeFromGame
func isActive() -> bool:
	return active

func _process(delta):
	update()

func _draw():
	if UserOptions.DebugDraw and regionOfInfluence:
		regionOfInfluence.draw(self as CanvasItem)
