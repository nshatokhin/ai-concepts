extends TriggerItemGiver
class_name TriggerHealthGiver

# the amount of health an entity receives when it runs over this trigger
export (float) var healthGiven : int = 100.0

func _init().():
	itemType = ItemType.Health
	
func _ready():
	addTriggerRegion(self, "_on_body_entered")

func _on_body_entered(body):
	if isActive():
		var health_func : FuncRef = funcref(body, "IncreaseHealth")
		
		if health_func.is_valid():
			health_func.call_func(healthGiven)
			Deactivate()
