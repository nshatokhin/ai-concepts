extends Weapon
class_name Blaster

const BlasterParams : Resource = preload("res://Weapons/Blaster/Blaster.tres")
export (PackedScene) var projectile = preload("res://Projectiles/BoltProjectile/BoltProjectile.tscn")

onready var muzzle : = $Muzzle

#--------------------------- ctor --------------------------------------------
#-----------------------------------------------------------------------------
func init(bot_agent):
	init_weapon(Weapon.WeaponType.Blaster, BlasterParams.DefaultRounds, BlasterParams.MaxRoundsCarried, BlasterParams.FiringFreq, BlasterParams.IdealRange, BlasterParams.MaxSpeed, bot_agent)
	# setup the fuzzy module
	InitializeFuzzyModule()

#------------------------------ ShootAt --------------------------------------
func ShootAt(pos : Vector2) -> void:
	if isReadyForNextShot():
		# fire!
		AddBolt(agent, pos);
		
		UpdateTimeWeaponIsNextAvailable()
		
		# add a trigger to the game so that the other bots can hear this shot
		# (provided they are within range)
		AddSound(agent, BlasterParams.SoundRange)

func AddBolt(shooter, pos) -> void:
	var bullet = projectile.instance()
	
	bullet.Init(shooter, pos, BlasterParams.MaxSpeed)
#		bullet.launcher = get_parent()
#		var direction = (muzzle.global_transform.origin - global_transform.origin).normalized()
#		bullet.rotation = direction.angle()
#		bullet.linear_velocity = direction * bullet_speed
#		bullet.global_position = muzzle.global_position
#		bullet.damage = damage
	bullet.global_position = muzzle.global_position
	get_node("/root").add_child(bullet)

#---------------------------- Desirability -----------------------------------
#
#-----------------------------------------------------------------------------
func GetDesirability(DistToTarget : float) -> float:
	# fuzzify distance and amount of ammo
	fuzzyModule.Fuzzify("DistToTarget", DistToTarget)
	
	lastDesirabilityScore = fuzzyModule.DeFuzzify("Desirability", FuzzyModule.DefuzzifyMethod.MaxAV)
	
	return lastDesirabilityScore


#----------------------- InitializeFuzzyModule -------------------------------
#
#  set up some fuzzy variables and rules
#-----------------------------------------------------------------------------
func InitializeFuzzyModule() -> void:
	var DistToTarget : FuzzyVariable = fuzzyModule.CreateFLV("DistToTarget")
	
	var Target_Close : FzSet = DistToTarget.AddLeftShoulderSet("Target_Close",0,25,150)
	var Target_Medium = DistToTarget.AddTriangularSet("Target_Medium",25,150,300)
	var Target_Far = DistToTarget.AddRightShoulderSet("Target_Far",150,300,1000)
	
	var Desirability : FuzzyVariable = fuzzyModule.CreateFLV("Desirability")
	var VeryDesirable : FzSet = Desirability.AddRightShoulderSet("VeryDesirable", 50, 75, 100)
	var Desirable : FzSet = Desirability.AddTriangularSet("Desirable", 25, 50, 75)
	var Undesirable : FzSet = Desirability.AddLeftShoulderSet("Undesirable", 0, 25, 50)
	
	fuzzyModule.AddRule(Target_Close, Desirable)
	fuzzyModule.AddRule(Target_Medium, FzVery.new(Undesirable))
	fuzzyModule.AddRule(Target_Far, FzVery.new(Undesirable))
