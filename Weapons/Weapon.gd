extends Node2D
class_name Weapon

#-----------------------------------------------------------------------------
#   Base Weapon class
#-----------------------------------------------------------------------------

enum WeaponType {
	Blaster = 0
	Shotgun = 1
	RailGun = 2
	RocketLauncher = 3
}

# a weapon is always (in this game) carried by a bot
var agent

# an enumeration indicating the type of weapon
var type : int

# fuzzy logic is used to determine the desirability of a weapon. Each weapon
# owns its own instance of a fuzzy module because each has a different rule 
# set for inferring desirability.
var fuzzyModule : FuzzyModule

# amount of ammo carried for this weapon
var numRoundsLeft : int

# maximum number of rounds a bot can carry for this weapon
var maxRoundsCarried : int

# the number of times this weapon can be fired per second
var rateOfFire : float

# the earliest time the next shot can be taken
var timeNextAvailable : float

# this is used to keep a local copy of the previous desirability score
# so that we can give some feedback for debugging
var lastDesirabilityScore : float

# this is the prefered distance from the enemy when using this weapon
var idealRange : float

# the max speed of the projectile this weapon fires
var maxProjectileSpeed : float

# The number of times a weapon can be discharges depends on its rate of fire.
# This method returns true if the weapon is able to be discharged at the 
# current time. (called from ShootAt() )
func isReadyForNextShot() -> bool:
	return Clock.GetCurrentTime() > timeNextAvailable

# this is called when a shot is fired to update timeNextAvailable
func UpdateTimeWeaponIsNextAvailable() -> void:
	timeNextAvailable = Clock.GetCurrentTime() + 1.0/rateOfFire

# this method initializes the fuzzy module with the appropriate fuzzy 
# variables and rule base.
func InitializeFuzzyModule() -> void:
	pass

func init_weapon(TypeOfGun : int, DefaultNumRounds : int, MaxRoundsCarried : int, RateOfFire : float, IdealRange : float, ProjectileSpeed : float, OwnerOfGun):
	type = TypeOfGun
	numRoundsLeft = DefaultNumRounds
	agent = OwnerOfGun
	rateOfFire = RateOfFire
	maxRoundsCarried = MaxRoundsCarried
	lastDesirabilityScore = 0
	idealRange = IdealRange
	maxProjectileSpeed = ProjectileSpeed
	timeNextAvailable = Clock.GetCurrentTime()
	fuzzyModule = FuzzyModule.new()


# this method aims the weapon at the given target by rotating the weapon's
# owner's facing direction (constrained by the bot's turning rate). It returns  
# true if the weapon is directly facing the target.
func AimAt(target : Vector2) -> bool:
	return agent.RotateFacingTowardPosition(target)

# this discharges a projectile from the weapon at the given target position
# (provided the weapon is ready to be discharged... every weapon has its
# own rate of fire)
func ShootAt(pos : Vector2) -> void:
	pass

# this method returns a value representing the desirability of using the
# weapon. This is used by the AI to select the most suitable weapon for
# a bot's current situation. This value is calculated using fuzzy logic
func GetDesirability(DistToTarget : float) -> float:
	return 0.0

# returns the number of rounds remaining for the weapon
func NumRoundsRemaining() -> int:
	return numRoundsLeft

func DecrementNumRounds() -> void:
	if numRoundsLeft > 0:
		--numRoundsLeft
		
func IncrementRounds(num : int) -> void:
	numRoundsLeft += num
	numRoundsLeft = clamp(numRoundsLeft, 0, maxRoundsCarried)

func AddSound(agent, soundRange : float):
	get_node("/root").add_child(TriggerSoundNotify.new(agent, soundRange, TriggerSoundNotify.SoundType.GunShoot))
