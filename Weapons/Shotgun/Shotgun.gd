extends Weapon
class_name Shotgun

const Params : Resource = preload("res://Weapons/Shotgun/Shotgun.tres")
export (PackedScene) var projectile = preload("res://Projectiles/ShellProjectile/ShellProjectile.tscn")

onready var muzzle : = $Muzzle

# how much shot the each shell contains
var numBallsInShell : int

# how much the shot spreads out when a cartridge is discharged
var spread : float

var rng : RandomNumberGenerator = RandomNumberGenerator.new()

#--------------------------- ctor --------------------------------------------
#-----------------------------------------------------------------------------
func init(bot_agent):
	rng.randomize()
	init_weapon(Weapon.WeaponType.Shotgun, Params.DefaultRounds, Params.MaxRoundsCarried, Params.FiringFreq, Params.IdealRange, Params.MaxSpeed, bot_agent)
	numBallsInShell = Params.NumBallsInShell
	spread = Params.Spread
	# setup the fuzzy module
	InitializeFuzzyModule()

#------------------------------ ShootAt --------------------------------------
func ShootAt(pos : Vector2) -> void:
	if NumRoundsRemaining() > 0 and isReadyForNextShot():
		# a shotgun cartridge contains lots of tiny metal balls called pellets. 
		# Therefore, every time the shotgun is discharged we have to calculate
		# the spread of the pellets and add one for each trajectory
		var pellets : Array = []
		for b in range(numBallsInShell):
			# determine deviation from target using a bell curve type distribution
			var deviation : float = rng.randi_range(0, spread) + rng.randi_range(0, spread) - spread
			#var angle : float = rng.randf_range(- spread / 2, spread / 2)
			var AdjustedTarget : Vector2 = pos
			
			# rotate the target vector by the deviation
			AdjustedTarget = AdjustedTarget.rotated(deviation)
			
			# add a pellet to the game world
			pellets.push_back(AddShotgunPellet(agent, AdjustedTarget))
			
		for pellet in pellets:
			for pellet2 in pellets:
				if pellet != pellet2:
					pellet.add_collision_exception_with(pellet2)
		
		numRoundsLeft -= 1
		
		UpdateTimeWeaponIsNextAvailable()
		
		# add a trigger to the game so that the other bots can hear this shot
		# (provided they are within range)
		AddSound(agent, Params.SoundRange)

func AddShotgunPellet(shooter, pos):
	var bullet = projectile.instance()
	
	bullet.Init(shooter, pos, muzzle.global_position, Params.MaxSpeed)
	bullet.global_position = muzzle.global_position
	get_node("/root").add_child(bullet)
	
	return bullet


#---------------------------- Desirability -----------------------------------
#
#-----------------------------------------------------------------------------
func GetDesirability(DistToTarget : float) -> float:
	if numRoundsLeft == 0:
		lastDesirabilityScore = 0
	else:
		# fuzzify distance and amount of ammo
		fuzzyModule.Fuzzify("DistanceToTarget", DistToTarget)
		fuzzyModule.Fuzzify("AmmoStatus", numRoundsLeft)
		
		lastDesirabilityScore = fuzzyModule.DeFuzzify("Desirability", FuzzyModule.DefuzzifyMethod.MaxAV)
	
	return lastDesirabilityScore;


#----------------------- InitializeFuzzyModule -------------------------------
#
#  set up some fuzzy variables and rules
#-----------------------------------------------------------------------------
func InitializeFuzzyModule() -> void:
	var DistanceToTarget : FuzzyVariable = fuzzyModule.CreateFLV("DistanceToTarget")
	
	var Target_Close : FzSet = DistanceToTarget.AddLeftShoulderSet("Target_Close", 0, 25, 150)
	var Target_Medium : FzSet = DistanceToTarget.AddTriangularSet("Target_Medium", 25, 150, 300)
	var Target_Far : FzSet = DistanceToTarget.AddRightShoulderSet("Target_Far", 150, 300, 1000)
	
	var Desirability : FuzzyVariable = fuzzyModule.CreateFLV("Desirability")
	
	var VeryDesirable : FzSet = Desirability.AddRightShoulderSet("VeryDesirable", 50, 75, 100)
	var Desirable : FzSet = Desirability.AddTriangularSet("Desirable", 25, 50, 75);
	var Undesirable : FzSet = Desirability.AddLeftShoulderSet("Undesirable", 0, 25, 50)
	
	var AmmoStatus : FuzzyVariable = fuzzyModule.CreateFLV("AmmoStatus")
	var Ammo_Loads : FzSet = AmmoStatus.AddRightShoulderSet("Ammo_Loads", 30, 60, 100)
	var Ammo_Okay : FzSet = AmmoStatus.AddTriangularSet("Ammo_Okay", 0, 30, 60)
	var Ammo_Low : FzSet = AmmoStatus.AddTriangularSet("Ammo_Low", 0, 0, 30)
	
	
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Loads), VeryDesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Okay), VeryDesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Low), VeryDesirable)
	
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Loads), VeryDesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Okay), Desirable)
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Low), Undesirable)
	
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Loads), Desirable)
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Okay), Undesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Low), Undesirable)
