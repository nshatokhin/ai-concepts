extends Weapon
class_name RocketLauncher

const Params : Resource = preload("res://Weapons/RocketLauncher/RocketLauncher.tres")
export (PackedScene) var projectile = preload("res://Projectiles/RocketProjectile/RocketProjectile.tscn")

onready var muzzle : = $Muzzle

#--------------------------- ctor --------------------------------------------
#-----------------------------------------------------------------------------
func init(bot_agent):
	init_weapon(Weapon.WeaponType.RocketLauncher, Params.DefaultRounds, Params.MaxRoundsCarried, Params.FiringFreq, Params.IdealRange, Params.MaxSpeed, bot_agent)
	# setup the fuzzy module
	InitializeFuzzyModule()

#------------------------------ ShootAt --------------------------------------
func ShootAt(pos : Vector2) -> void:
	if isReadyForNextShot():
		# fire!
		AddRocket(agent, pos);
		
		UpdateTimeWeaponIsNextAvailable()
		
		# add a trigger to the game so that the other bots can hear this shot
		# (provided they are within range)
		AddSound(agent, Params.SoundRange)

func AddRocket(shooter, pos) -> void:
	var rocket = projectile.instance()
	
	rocket.Init(shooter, pos, Params.MaxSpeed)

	rocket.global_position = muzzle.global_position
	get_node("/root").add_child(rocket)

#---------------------------- Desirability -----------------------------------
#
#-----------------------------------------------------------------------------
func GetDesirability(DistToTarget : float) -> float:
	if numRoundsLeft == 0:
		lastDesirabilityScore = 0
	else:
		# fuzzify distance and amount of ammo
		fuzzyModule.Fuzzify("DistToTarget", DistToTarget)
		fuzzyModule.Fuzzify("AmmoStatus", numRoundsLeft)
		
		lastDesirabilityScore = fuzzyModule.DeFuzzify("Desirability", FuzzyModule.DefuzzifyMethod.MaxAV)
	
	return lastDesirabilityScore


#----------------------- InitializeFuzzyModule -------------------------------
#
#  set up some fuzzy variables and rules
#-----------------------------------------------------------------------------
func InitializeFuzzyModule() -> void:
	var DistToTarget : FuzzyVariable = fuzzyModule.CreateFLV("DistToTarget")
	
	var Target_Close : FzSet = DistToTarget.AddLeftShoulderSet("Target_Close",0,25,150)
	var Target_Medium : FzSet = DistToTarget.AddTriangularSet("Target_Medium",25,150,300)
	var Target_Far : FzSet = DistToTarget.AddRightShoulderSet("Target_Far",150,300,1000)
	
	var Desirability : FuzzyVariable = fuzzyModule.CreateFLV("Desirability") 
	var VeryDesirable : FzSet = Desirability.AddRightShoulderSet("VeryDesirable", 50, 75, 100)
	var Desirable : FzSet = Desirability.AddTriangularSet("Desirable", 25, 50, 75)
	var Undesirable : FzSet = Desirability.AddLeftShoulderSet("Undesirable", 0, 25, 50)
	
	var AmmoStatus : FuzzyVariable = fuzzyModule.CreateFLV("AmmoStatus")
	var Ammo_Loads : FzSet = AmmoStatus.AddRightShoulderSet("Ammo_Loads", 10, 30, 100)
	var Ammo_Okay : FzSet = AmmoStatus.AddTriangularSet("Ammo_Okay", 0, 10, 30)
	var Ammo_Low : FzSet = AmmoStatus.AddTriangularSet("Ammo_Low", 0, 0, 10)
	
	
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Loads), Undesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Okay), Undesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Close, Ammo_Low), Undesirable)
	
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Loads), VeryDesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Okay), VeryDesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Medium, Ammo_Low), Desirable)
	
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Loads), Desirable)
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Okay), Undesirable)
	fuzzyModule.AddRule(FzAND.new(Target_Far, Ammo_Low), Undesirable)
