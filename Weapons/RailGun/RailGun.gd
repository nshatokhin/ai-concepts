extends Weapon
class_name RailGun

const Params : Resource = preload("res://Weapons/RailGun/RailGun.tres")
export (PackedScene) var projectile = preload("res://Projectiles/SlugProjectile/SlugProjectile.tscn")

onready var muzzle : = $Muzzle

#--------------------------- ctor --------------------------------------------
#-----------------------------------------------------------------------------
func init(bot_agent):
	init_weapon(Weapon.WeaponType.RailGun, Params.DefaultRounds, Params.MaxRoundsCarried, Params.FiringFreq, Params.IdealRange, Params.MaxSpeed, bot_agent)
	# setup the fuzzy module
	InitializeFuzzyModule()

#------------------------------ ShootAt --------------------------------------
func ShootAt(pos : Vector2) -> void:
	if isReadyForNextShot():
		# fire!
		var space_state : Physics2DDirectSpaceState = agent.get_world_2d().direct_space_state
		var intersection : Dictionary = space_state.intersect_ray(muzzle.global_position, pos, [agent])
		
		if not intersection.empty():
			var collider = intersection.collider
			var position : Vector2 = intersection.position
			
			AddRailGunSlug(agent, position)
			
			print("Hit: ", collider.name)

			var damage_func : FuncRef = funcref(collider, "add_damage")
		
			if damage_func.is_valid():
				damage_func.call_func(Params.Damage, agent)
			
		else:
			# to outside screen
			pass
		
		UpdateTimeWeaponIsNextAvailable()
		
		# add a trigger to the game so that the other bots can hear this shot
		# (provided they are within range)
		AddSound(agent, Params.SoundRange)

func AddRailGunSlug(shooter, pos) -> void:
	var slug = projectile.instance()
	
	slug.Init(shooter, pos, muzzle.global_position)
	#slug.global_position = muzzle.global_position
	get_node("/root").add_child(slug)

#---------------------------- Desirability -----------------------------------
#
#-----------------------------------------------------------------------------
func GetDesirability(DistToTarget : float) -> float:
	# fuzzify distance and amount of ammo
	fuzzyModule.Fuzzify("DistToTarget", DistToTarget)
	
	lastDesirabilityScore = fuzzyModule.DeFuzzify("Desirability", FuzzyModule.DefuzzifyMethod.MaxAV)
	
	return lastDesirabilityScore


#----------------------- InitializeFuzzyModule -------------------------------
#
#  set up some fuzzy variables and rules
#-----------------------------------------------------------------------------
func InitializeFuzzyModule() -> void:
	var DistToTarget : FuzzyVariable = fuzzyModule.CreateFLV("DistToTarget")
	
	var Target_Close : FzSet = DistToTarget.AddLeftShoulderSet("Target_Close",0,25,150)
	var Target_Medium = DistToTarget.AddTriangularSet("Target_Medium",25,150,300)
	var Target_Far = DistToTarget.AddRightShoulderSet("Target_Far",150,300,1000)
	
	var Desirability : FuzzyVariable = fuzzyModule.CreateFLV("Desirability")
	var VeryDesirable : FzSet = Desirability.AddRightShoulderSet("VeryDesirable", 50, 75, 100)
	var Desirable : FzSet = Desirability.AddTriangularSet("Desirable", 25, 50, 75)
	var Undesirable : FzSet = Desirability.AddLeftShoulderSet("Undesirable", 0, 25, 50)
	
	fuzzyModule.AddRule(Target_Close, Desirable)
	fuzzyModule.AddRule(Target_Medium, FzVery.new(Undesirable))
	fuzzyModule.AddRule(Target_Far, FzVery.new(Undesirable))
