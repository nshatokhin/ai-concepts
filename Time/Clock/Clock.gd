extends Node

#------------------------------------------------------------------------
#   Timer to measure time in seconds
#------------------------------------------------------------------------

# set to the time (in seconds) when class is instantiated
var startTime : float

# set the start time
func _ready():
	startTime = OS.get_system_time_msecs() * 0.001

# returns how much time has elapsed since the timer was started
func GetCurrentTime() -> float:
	return OS.get_system_time_msecs() * 0.001 - startTime
