extends Reference
class_name Regulator

#------------------------------------------------------------------------
#
#  Use this class to regulate code flow (for an update function say)
#          Instantiate the class with the frequency you would like your code
#          section to flow (like 10 times per second) and then only allow 
#          the program flow to continue if Ready() returns true
#
#------------------------------------------------------------------------

# the time period between updates 
var updatePeriod : float

# the next time the regulator allows code flow
var nextUpdateTime : int

var rng = RandomNumberGenerator.new()
  
func _init(NumUpdatesPerSecondRqd : float):
	rng.randomize()
	nextUpdateTime = OS.get_system_time_msecs() + rng.randf() * 1000
	
	if NumUpdatesPerSecondRqd > 0:
		updatePeriod = 1000.0 / NumUpdatesPerSecondRqd
	elif Utils.isEqual(0.0, NumUpdatesPerSecondRqd):
		updatePeriod = 0.0
	
	elif NumUpdatesPerSecondRqd < 0:
		updatePeriod = -1.0


# returns true if the current time exceeds nextUpdateTime
func isReady() -> bool:
	# if a regulator is instantiated with a zero freq then it goes into
	# stealth mode (doesn't regulate)
	if Utils.isEqual(0.0, updatePeriod):
		return true
	
	# if the regulator is instantiated with a negative freq then it will
	# never allow the code to flow
	if updatePeriod < 0.0:
		return false
	
	var CurrentTime : int = OS.get_system_time_msecs()
	
	# the number of milliseconds the update period can vary per required
	# update-step. This is here to make sure any multiple clients of this class
	# have their updates spread evenly
	var UpdatePeriodVariator : float = 10.0
	
	if CurrentTime >= nextUpdateTime:
		nextUpdateTime = CurrentTime + updatePeriod + rng.randf_range(-UpdatePeriodVariator, UpdatePeriodVariator)
	
		return true
	
	return false
